import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';

import store from 'state/store';
import AppContainer from 'ui/AppContainer';
import 'assets/styles/index.scss';

import 'app-init/bootstrap';
import 'app-init/fontawesome';
import 'whatwg-fetch';//Fixme che cos'è?? xD

ReactDOM.render(
  <Provider store={store}>
    <AppContainer />
  </Provider>,
  document.getElementById('bdc_impianti'),
);
