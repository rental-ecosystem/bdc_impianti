/* istanbul ignore next */

import { library } from '@fortawesome/fontawesome-svg-core';
import {
  faChevronRight,
  faChevronLeft,
  faCheck,
  faInfoCircle,
  faTrash,
  faDownload,
  faTimes,
  faCalendar,
  faCircle,

} from '@fortawesome/free-solid-svg-icons';

const icons = [
  faChevronRight,
  faChevronLeft,
  faCheck,
  faInfoCircle,
  faTrash,
  faDownload,
  faTimes,
  faCalendar,
  faCircle,
];

library.add(...icons);
