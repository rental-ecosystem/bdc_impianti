import {store} from 'state/store'
import {getToken} from "state/user/selectors";

const {
  REACT_APP_DOMAIN,
  REACT_APP_CLIENT_ID,
  REACT_APP_CLIENT_SECRET,
} = process.env;

const config = {
  username: 'admin',
  password: 'adminadmin',
  grant_type: 'password',
  client_id: REACT_APP_CLIENT_ID,
  client_secret: REACT_APP_CLIENT_SECRET,
};

export const getTokenOAuth2 = async () => {

  const state = store.getState();
  const token = getToken(state);

  if (!token) {
    await fetch(`${REACT_APP_DOMAIN}/oauth/token`, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Authorization: `Basic ${btoa(`${REACT_APP_CLIENT_ID}:${REACT_APP_CLIENT_SECRET}`)}`,
      },
      body: Object
        .keys(config)
        .map(key => `${encodeURIComponent(key)}=${encodeURIComponent(config[key])}`)
        .join('&'),
    }).then((res) => {
      if (res.ok) {
        res.json().then((json) => {
          return json;
        });
      }
    }).catch(() => {
      console.log(`Errore nell' acquisizione del token`);
    });
  } else {
    return token;
  }
};

export const fetchEndPoint = async (url, method, data, useMetadata) => {

  await getTokenOAuth2();

  const state = store.getState();
  const token = getToken(state);

  if (token) {

    const objRequest = {
      method,
      headers: {
        'Content-Type': 'application/json',
        Authorization: `Bearer ${token}`,
        'Accept-Language': 'it',
      },
      body: JSON.stringify(data)
    };

    if (!data) {
      delete objRequest.body;
    }

    await fetch(`${REACT_APP_DOMAIN}${url}`, objRequest).then(res => {

      if (res && res.ok) {
        let contentType = res.headers.get("content-type");
        if (contentType && contentType.includes("application/json")) {
          res.json();
        } else {
          console.log("Oops, we haven't got JSON!");
        }
        return res;
      }
      throw new Error("Network response was not ok.");

    }).then(json => {
      if (json.errors.length) {
        throw json.errors
      }
      if (useMetadata) {
        return {payload: json.payload, metaData: json.metaData}
      }
      return {payload: json.payload};
    })
      .catch((error) => {
        console.log(error);
      });
  } else {
    throw new Error('FetchEndPoint: No Token found')
  }

};
