import { connect } from 'react-redux';
import App from 'ui/App';

const mapStateToProps = state => ({
});

const AppContainer = connect(mapStateToProps)(App);
export default AppContainer;
