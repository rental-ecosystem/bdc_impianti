import {connect} from 'react-redux';
import RiepilogoSito from "ui/RiepilogoSito/components/RiepilogoSito";

export const mapStateToProps = (state, ownProps) => ({
});

export const mapDispatchToProps = dispatch => ({
});

const RiepilogoSitoContainer = connect(mapStateToProps, mapDispatchToProps)(RiepilogoSito);

export default RiepilogoSitoContainer;
