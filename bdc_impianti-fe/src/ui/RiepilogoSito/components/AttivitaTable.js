import React, {Fragment} from 'react';
import {Table} from "reactstrap";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import '../assets/AttivitaTable.scss';
import PropTypes from "prop-types";

const AttivitaTable = (props) => {

  const dot = (priorita) => <FontAwesomeIcon icon="circle" className={`attivitaTable__valutazione-${priorita}`} />;

  const findValutazione = (key) => {
    const valutazione = props.valutazioni.find(valutazione => valutazione.id === key);
    if (valutazione) return dot(valutazione.prioritaVal);
  };

  return (
    <Table bordered>
      <thead>
      <tr>
        <th>Rif. Cod.</th>
        <th>Descrizione</th>
        <th>Impatto</th>
        <th>Valutazione</th>
        <th>note</th>
      </tr>
      </thead>
      <tbody>
      {props.macroAttivitaList ?
        props.macroAttivitaList.map((macroAttivitaObject) =>
          <Fragment key={macroAttivitaObject.rifCod}>
            <tr>
              <th>
                {macroAttivitaObject.rifCod}
              </th>
              <th colSpan={4}>
                {macroAttivitaObject.intestazione}
              </th>
            </tr>
            {macroAttivitaObject.records.map((attivita, index) =>
              <tr key={index}>
                <td>{attivita.codiceRifAtt}</td>
                <td>{attivita.descrizioneAtt}</td>
                <td>{attivita.impattoAtt}</td>
                <td>{(findValutazione(attivita.fkValutazioneAtt))}</td>
                <td>{attivita.noteAttivita}</td>
              </tr>
            )}
          </Fragment>
        )
        : null
      }
      </tbody>
    </Table>
  )
};

AttivitaTable.propTypes = {
  macroAttivitaList: PropTypes.array.isRequired,
  valutazioni: PropTypes.array.isRequired
};

export default AttivitaTable;
