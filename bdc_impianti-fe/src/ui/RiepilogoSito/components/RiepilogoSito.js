import React from 'react';
import AttivitaTable from "./AttivitaTable";

const RiepilogoSito = (props) => {

  const valutazioni = [
    {
      id: 1,
      prioritaVal: 1,
      descrizioneVal: 'PIENAMENTE SODDISFACENTE'
    },
    {
      id: 2,
      prioritaVal: 3,
      descrizioneVal: 'NON SODDISFACENTE'
    },
    {
      id: 3,
      prioritaVal: 2,
      descrizioneVal: 'SODDISFACENTE'
    },
    {
      id: 4,
      prioritaVal: 4,
      descrizioneVal: 'N/A - Asset non presente o attività non prevista'
    },
  ];

  const macroAttivitaList = [
    {
      intestazione: 'ACCESSO - Aree esterne e prossime alla recinzione',
      rifCod: '1',
      records: [
        {
          idAttivita: 1,
          codiceRifAtt: '1.1',
          descrizioneAtt: 'Verifica e pulizia dell\'area immediatamente antistante la pertinenza della SRB per consentire accesso in sicurezza',
          impattoAtt: '3',
          noteAttivita: '',
          fkValutazioneAtt: 1,
          fkAuditAtt: 1
        },
        {
          idAttivita: 2,
          codiceRifAtt: '1.2',
          descrizioneAtt: 'Verifica presenza chiavi',
          impattoAtt: '4',
          noteAttivita: 'LUCCHETTO',
          fkValutazioneAtt: 4,
          fkAuditAtt: 1
        }
      ]
    },
    {
      intestazione: 'ACCESSO - Aree interne alla recinzione ',
      rifCod: '2',
      records: [
        {
          idAttivita: 4,
          codiceRifAtt: '2.1',
          descrizioneAtt: 'Verifica presenza materiali di risulta e/o oggetti abbandonati',
          impattoAtt: '3',
          noteAttivita: '',
          fkValutazioneAtt: 1,
          fkAuditAtt: 1
        }
      ]
    }
  ];

  return (
    <>
      DATI GENERALI
      <AttivitaTable macroAttivitaList={macroAttivitaList} valutazioni={valutazioni} />
    </>
  )
};

RiepilogoSito.propTypes = {};

export default RiepilogoSito;
