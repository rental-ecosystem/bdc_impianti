import React from 'react';
import '../App.css';
import ElencoImpiantiContainer from "./ElencoImpianti/containers/ElencoImpiantiContainer";

function App() {
  return (
    <div className="App">
      <ElencoImpiantiContainer />
    </div>
  );
}

export default App;
