import { connect } from 'react-redux';
import ElencoImpianti from "../components/ElencoImpianti.js";
import {setItemsPerPage} from "../state/actions"

const impianti = [
  {checked: false, nomeSito: "Impianto1",  zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'OK', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto2",  zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'OK', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto3",  zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'OK', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto4",  zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'KO', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto5",  zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'OK', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto6",  zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'OK', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto7",  zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'KO', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto8",  zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'OK', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto9",  zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'OK', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto10", zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'KO', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto11", zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'OK', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto12", zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'OK', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto13", zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'OK', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto14", zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'OK', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto15", zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'KO', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto16", zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'OK', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto17", zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'OK', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto18", zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'OK', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto19", zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'OK', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto20", zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'OK', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto21", zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'KO', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto22", zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'OK', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto23", zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'OK', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto24", zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'OK', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto25", zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'OK', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto26", zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'OK', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto27", zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'KO', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto28", zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'OK', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto29", zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'OK', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
  {checked: false, nomeSito: "Impianto30", zona: 'Zona', regione: 'Regione', provincia: 'Provincia', codice: 'Codice', ispettore: 'NomeCognome isp.', dataAudit: 'dataAudit', esito: 'OK', op1: 'Modifica', op2: 'Aggiungi', op3: 'Rimuovi'},
];


export const mapStateToProps = () => ({
  impianti: impianti,
});

export const mapDispatchToProps = dispatch => ({
  setItemsPerPage: (ipp) => dispatch(setItemsPerPage(ipp)),
});

const RiepilogoSitoContainer = connect(mapStateToProps, mapDispatchToProps)(ElencoImpianti);

export default RiepilogoSitoContainer;
