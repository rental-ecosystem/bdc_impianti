import React from 'react';
import {Button, Input, Table} from "reactstrap";

const RicercaImpianti = () => {

  const [nome, setNome] = React.useState('');
  const [codice, setCodice] = React.useState('');
  return (
    <div className="RicercaImpianti">
      <Table bordered striped>
        <thead>
        <tr>
          <th>Nome</th>
          <th>Codice</th>
          <th/>
        </tr>
        </thead>
        <tbody>
        <tr>
          <td>
            <Input
              value={nome}
              onChange={e => setNome(e.target.value)}
            />
          </td>
          <td>
            <Input
              value={codice}
              onChange={e => setCodice(e.target.value)}
            />
          </td>
          <td>
            <Button block>Cerca</Button>
          </td>
        </tr>
        </tbody>
      </Table>
    </div>
  );
};

export default RicercaImpianti;
