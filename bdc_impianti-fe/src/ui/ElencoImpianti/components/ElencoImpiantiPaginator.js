import React from 'react';
import PropTypes from 'prop-types';
import {
  ButtonDropdown,
  DropdownItem,
  DropdownMenu,
  DropdownToggle,
  Pagination,
  PaginationItem,
  PaginationLink
} from 'reactstrap';

const ElencoImpiantiPaginator = ({totalItems, itemsPerPage, setItemsPerPage, setCurrentPage}) => {

  const [toggle, handleToggle] = React.useState(false);

  let pageNumbers = [];
  for(let i = 1; i <= Math.ceil(totalItems / itemsPerPage); i++) {
    pageNumbers.push(i)
  }

  return (
    <div style={{marginTop: '1em'}}>
      <Pagination>
        {
          pageNumbers && pageNumbers.map(number => {
            return (
              <PaginationItem key={number}>
                <PaginationLink
                  onClick={() => setCurrentPage(number)}
                  // href="!#"
                >
                  {number}
                </PaginationLink>
              </PaginationItem>
            )
          })}
      </Pagination>
      <ButtonDropdown
        isOpen={toggle}
        toggle={() => handleToggle(!toggle)}
        direction="down"
      >
        <DropdownToggle caret>
          {itemsPerPage}
        </DropdownToggle>
        <DropdownMenu>
          <DropdownItem onClick={() => setItemsPerPage(10)}>10</DropdownItem>
          <DropdownItem onClick={() => setItemsPerPage(20)}>20</DropdownItem>
          <DropdownItem onClick={() => setItemsPerPage(50)}>50</DropdownItem>
        </DropdownMenu>
      </ButtonDropdown>
    </div>
  );

};

ElencoImpiantiPaginator.propTypes = {
  totalItems: PropTypes.number.isRequired,
  itemsPerPage: PropTypes.number.isRequired,
  setItemsPerPage: PropTypes.func.isRequired,
  setCurrentPage: PropTypes.func.isRequired
};

export default ElencoImpiantiPaginator;
