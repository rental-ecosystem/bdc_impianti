import React, {Component} from 'react';
import {Button, Table} from "reactstrap";
import 'assets/styles/ElencoImpianti/ElencoImpianti.scss';
import ElencoImpiantiPaginator from "./ElencoImpiantiPaginator";
import RicercaImpianti from "./RicercaImpianti";
// import PropTypes from 'prop-types';


class ElencoImpianti extends Component {
  constructor(props) {
    super(props);
    this.state = {
      impianti: props.impianti,
      paginationConfig: {
        currentPage: 1,
        itemsPerPage: 10
      },
      currentItems: []
    };
    this.setStateImpianti = this.setStateImpianti.bind(this);
    this.handlePagination = this.handlePagination.bind(this);
  }

  setStateImpianti(impianti){
    this.setState({currentItems: impianti})
  }

  handlePagination(){
    const {paginationConfig: {currentPage, itemsPerPage}, impianti} = this.state;
    const indexOfLastItem = currentPage * itemsPerPage;
    const indexOfFirstItem = indexOfLastItem - itemsPerPage;
    const currentItems = impianti.slice(indexOfFirstItem, indexOfLastItem);
    this.setStateImpianti(currentItems);
  };

  componentDidMount() {
    this.handlePagination()
  }

  setItemsPerPage(ipp){
    const {paginationConfig} = this.state;
    this.setState({
      paginationConfig: {
        currentPage: paginationConfig.currentPage,
        itemsPerPage: ipp
      }
    }, () => this.handlePagination())
  }

  setCurrentPage(p){
    const {paginationConfig} = this.state;
    this.setState({
      paginationConfig: {
        currentPage: p,
        itemsPerPage: paginationConfig.itemsPerPage
      }
    }, () => this.handlePagination())
  }

  render() {

    const {
      currentItems,
      paginationConfig: {itemsPerPage}
    } = this.state;
    const {impianti} = this.state;

    return (
      <div className="ElencoImpianti">
        <h4>Elenco siti</h4>

        <RicercaImpianti/>

        <Table
          responsive
          bordered
          hover
          size="sm"
        >
          <thead>
            <tr>
              <th>Seleziona</th>
              <th>Nome</th>
              <th>Codice</th>
              <th>
                <tr>
                  <th colSpan={5}>
                    ULTIMO AUDIT
                  </th>
                </tr>
                <tr>
                  <th>Data audit</th>
                  <th>Ispettore</th>
                  <th>Zona</th>
                  <th>Regione</th>
                  <th>Provincia</th>
                </tr>
              </th>
              <th>Esito</th>
              <th/>
            </tr>
          </thead>
          <tbody>
          {currentItems && currentItems.map((imp, i) => {
            return <tr key={i}>
              <td>
                <input
                  type="checkbox"
                  defaultChecked={imp.checked}
                />
              </td>
              <td>{imp.nomeSito}</td>
              <td>{imp.codice}</td>
              <td>
                <tr>
                  <td>{imp.dataAudit}</td>
                  <td>{imp.ispettore}</td>
                  <td>{imp.zona}</td>
                  <td>{imp.regione}</td>
                  <td>{imp.provincia}</td>
                </tr>
              </td>
              <td>{imp.esito}</td>
              <td>
                <Button
                  size="sm"
                  onClick={() => alert('You just clicked function1!')}
                >{imp.op1}
                </Button>{' '}
                <Button
                  size="sm"
                  onClick={() => alert('You just clicked function2!')}
                >{imp.op2}
                </Button>{' '}
                <Button
                  size="sm"
                  onClick={() => alert('You just clicked function3!')}
                >{imp.op3}
                </Button>
              </td>
            </tr>
          })}
          </tbody>
        </Table>
        <ElencoImpiantiPaginator
          totalItems={impianti.length}
          itemsPerPage={itemsPerPage}
          setItemsPerPage={this.setItemsPerPage.bind(this)}
          setCurrentPage={this.setCurrentPage.bind(this)}
        />
      </div>
    );
  }
}

// ElencoImpianti.propTypes = {
//
// };

export default ElencoImpianti;
