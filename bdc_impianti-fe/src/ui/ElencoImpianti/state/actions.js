import {
  SET_ITEMS,
  SET_CURRENT_PAGE,
  SET_ITEMS_PER_PAGE
} from "./types";


export const setItems = (items) => ({
  type: SET_ITEMS,
  payload: items
});

export const setCurrentPage = (page) => ({
  type: SET_CURRENT_PAGE,
  payload: page
});

export const setItemsPerPage = (ipp) => ({
  type: SET_ITEMS_PER_PAGE,
  payload: ipp
});