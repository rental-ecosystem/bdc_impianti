import React from 'react';
import {Table} from "reactstrap";

const RiepilogoSito = (props) => {

  const valutazioni = [
    {
      priorita: 0,
      descrizione: 'PIENAMENTE SODDISFACENTE'
    },
    {
      priorita: 2,
      descrizione: 'NON SODDISFACENTE'
    },
    {
      priorita: 1,
      descrizione: 'SODDISFACENTE'
    },
    {
      priorita: 3,
      descrizione: 'N/A - Asset non presente o attività non prevista'
    },
  ].sort((a, b) => a.priorita - b.priorita);

  return (
    <Table bordered>
      <thead>
      <tr>
        <th>Rif. Cod.</th>
        <th>Descrizione</th>
        <th>Impatto</th>
        {
          valutazioni ?
            valutazioni.map((valutazione, index) => {
              return <th key={index}>{valutazione.descrizione}</th>
            })
            : null
        }
        <th>note</th>
      </tr>
      </thead>
      <tbody>
      <tr>
        <th scope="row">1</th>
        <td>Mark</td>
        <td>Otto</td>
        <td>@mdo</td>
      </tr>
      <tr>
        <th scope="row">2</th>
        <td>Jacob</td>
        <td>Thornton</td>
        <td>@fat</td>
      </tr>
      <tr>
        <th scope="row">3</th>
        <td>Larry</td>
        <td>the Bird</td>
        <td>@twitter</td>
      </tr>
      </tbody>
    </Table>
  )
};

RiepilogoSito.propTypes = {};

export default RiepilogoSito;
