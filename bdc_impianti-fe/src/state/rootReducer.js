import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';

import user from 'state/user/reducer';
import elencoImpianti from '../ui/ElencoImpianti/state/reducer';

export default combineReducers({
  form,
  user,
  elencoImpianti
});
