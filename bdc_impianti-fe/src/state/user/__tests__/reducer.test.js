import reducer from 'state/user/reducer';
import {
  loginUser,
  logoutUser,
} from 'state/user/actions';

let state;

describe('state/user reducer', () => {
  it('returns the default state', () => {
    state = reducer();
    expect(state).toEqual(expect.any(Object));
    expect(state).toHaveProperty('username', undefined);
    expect(state).toHaveProperty('token', undefined);
    expect(state).toHaveProperty('refresh', undefined);
    expect(state).toHaveProperty('permissions', undefined);
  });

  it('returns a default state with user and token if the window variables are defined', () => {
    window.loggedUser = 'my test';
    window.loggedUserToken = 'other-token';
    window.loggedUserRefreshToken = 'refresh-token';
    window.user = {
      permissions: {
        smeUser: true,
      },
    };
    const prefilledState = reducer();
    expect(prefilledState).toEqual(expect.any(Object));
    expect(prefilledState).toHaveProperty('username', 'my test');
    expect(prefilledState).toHaveProperty('token', 'other-token');
    expect(prefilledState).toHaveProperty('refresh', 'refresh-token');
    expect(prefilledState).toHaveProperty('permissions', { smeUser: true });

    delete window.loggedUser;
    delete window.loggedUserToken;
    delete window.loggedUserRefreshToken;
    delete window.user;
  });

  describe('after the loginUser action', () => {
    it('adds the username and token to the state', () => {
      window.user = {
        permissions: {
          smeUser: true,
        },
      };
      state = reducer(state, loginUser('me', 'asdf-asdf', 'codiceFiscale', 'refresh-as'));
      expect(state).toHaveProperty('username', 'me');
      expect(state).toHaveProperty('token', 'asdf-asdf');
      expect(state).toHaveProperty('codiceFiscale', 'codiceFiscale');
      expect(state).toHaveProperty('refresh', 'refresh-as');

      delete window.user;
    });

    describe('after the logoutUser action', () => {
      it('resets the user state', () => {
        state = reducer(state, logoutUser());
        expect(state).toHaveProperty('username', null);
        expect(state).toHaveProperty('token', null);
        expect(state).toHaveProperty('refresh', null);
      });
    });
  });
});
