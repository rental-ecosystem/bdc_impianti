import {
  loginUser,
  logoutUser,
} from 'state/user/actions';
import {
  LOGIN_USER,
  LOGOUT_USER,
} from 'state/user/types';


window.loggedUser = 'user';
window.loggedUserToken = 'userToken';
window.user = {
  codiceFiscale: 'codiceFiscale',
  permissions: {},

};


describe('state/user actions', () => {
  describe('loginUser', () => {
    it('returns the correct action', () => {
      const action = loginUser('user', 'asdf-asdf');
      expect(action).toHaveProperty('type', LOGIN_USER);
      expect(action).toHaveProperty('payload', expect.any(Object));
    });
  });

  describe('logoutUser', () => {
    it('returns the correct action', () => {
      const action = logoutUser();
      expect(action).toHaveProperty('type', LOGOUT_USER);
      expect(action).not.toHaveProperty('payload');
    });
  });
});
