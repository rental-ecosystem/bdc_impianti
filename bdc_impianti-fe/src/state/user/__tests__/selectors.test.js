import {
  getUser,
  getUsername,
  getToken,
  getRefreshToken,
  getCodiceFiscale,
  isLoggedIn,
  getPermissionsIsSmeUser,
} from 'state/user/selectors';

const state = {
  user: {
    username: 'me',
    token: 'asdf-asfd',
    codiceFiscale: 'codicefiscle01',
    refresh: 'refresh-token',
    permissions: {
      smeUser: true,
    },
  },
};

describe('state/user selectors', () => {
  describe('getUser', () => {
    it('returns the complete user state', () => {
      expect(getUser(state)).toBe(state.user);
    });
  });

  describe('getUsername', () => {
    it('returns the username', () => {
      expect(getUsername(state)).toBe(state.user.username);
    });
  });

  describe('getToken', () => {
    it('returns the token', () => {
      expect(getToken(state)).toBe(state.user.token);
    });
  });

  describe('getRefreshToken', () => {
    it('returns the refresh token', () => {
      expect(getRefreshToken(state)).toBe(state.user.refresh);
    });
  });

  describe('getCodiceFiscale', () => {
    it('returns the codiceFiscale', () => {
      expect(getCodiceFiscale(state)).toBe(state.user.codiceFiscale);
    });
  });

  describe('getPermissionsIsSmeUser', () => {
    it('returns the getPermissionsIsSmeUser', () => {
      expect(getPermissionsIsSmeUser(state)).toBe(true);
    });
  });

  describe('isLoggedIn', () => {
    it('returns true if the username exists', () => {
      expect(isLoggedIn(state)).toBe(true);
    });

    it('returns false if the username is not set', () => {
      expect(isLoggedIn({
        user: {
          ...state.user,
          username: null,
        },
      })).toBe(false);
    });
  });
});
