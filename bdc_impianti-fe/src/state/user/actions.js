import {
  LOGIN_USER,
  LOGOUT_USER,
} from 'state/user/types';


export const loginUser = (username, token, refresh) => ({
  type: LOGIN_USER,
  payload: {
    username,
    token,
    refresh,
  },
});

export const logoutUser = () => ({
  type: LOGOUT_USER,
});
