import { createSelector } from 'reselect';

export const getUser = state => state.user;

export const getUsername = createSelector(
  getUser,
  user => user.username,
);

export const getToken = createSelector(
  getUser,
  user => user.token,
);

export const getRefreshToken = createSelector(
  getUser,
  user => user.refresh,
);

export const isLoggedIn = createSelector(
  getUsername,
  username => !!username,
);
