import {
  LOGIN_USER,
  LOGOUT_USER,
} from 'state/user/types';

const defaultState = () => {

  return {
    username: window.loggedUser,
    token: window.loggedUserToken,
    refresh: window.loggedUserRefreshToken,
    permissions: window.user && window.user.permissions,
  };
};


const reducer = (state = defaultState(), action = {}) => {
  switch (action.type) {
    case LOGIN_USER:
      return {
        ...action.payload,
        permissions: {
          ...window.user.permissions,
        },
      };
    case LOGOUT_USER:
      return {
        username: null,
        token: null,
        refresh: null,
      };
    default:
      return state;
  }
};

export default reducer;
