package it.keybiz.aps.controller.checkList;

import java.io.IOException;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import org.entando.entando.aps.system.services.storage.LocalStorageManager;
import org.entando.entando.web.common.model.RestResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.util.StringUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.agiletec.aps.system.SystemConstants;

import it.keybiz.aps.service.checkList.CheckListService;

@Validated
@RestController
@CrossOrigin(origins = "http://localhost:3000")
@RequestMapping(value = "checklist")
public class CheckListController implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 7169551438464571112L;
	private static final Logger log = LoggerFactory.getLogger(CheckListController.class);

	@Autowired
	private CheckListService checkListService;

	@Autowired
	@Qualifier(SystemConstants.STORAGE_MANAGER)
	private LocalStorageManager localStorageManager;

	@PostMapping("/importData")
	public RestResponse<Boolean, Map<String, String>> importData(@RequestParam("file") MultipartFile excelFile) {
		if (!StringUtils.endsWithIgnoreCase(excelFile.getOriginalFilename(), "xlsm")) {
			// TODO
		}
		try {
			checkListService.importData(excelFile.getInputStream());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return new RestResponse<Boolean, Map<String, String>>(true, new HashMap<String, String>());
	}
}
