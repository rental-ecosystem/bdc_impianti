package it.keybiz.aps.utils;

public class Const {

	//importData - dati generali (sito/audit)
	public static final String data_audit = "data";
	public static final String nominativo_ispettore = "ispettore";
	public static final String zona_sito = "zona";
	public static final String regione_sito = "regione";
	public static final String provincia_sito= "provincia";
	public static final String codice_sito= "codice";
	public static final String nome_sito= "nome";

	//importdata - attivita'
	public static final String rifCod_attivita = "cod.";	
	public static final String desc_attivita = "descrizione";
	public static final String impatto_attivita = "impatto";
	public static final String note_attivita = "note";
	
	//DBConst - table 'valutazioneattivita'
	public static final int pienamenteSoddisfacente_valutazione = 1;
	public static final int soddisfacente_valutazione = 2;
	public static final int nonSoddisfacente_valutazione = 3;	
	public static final int na_valutazione = 4;

}
