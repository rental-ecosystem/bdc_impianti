package it.keybiz.aps.service.checkList;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.DateUtil;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.entando.entando.plugins.jpattivita.aps.system.services.attivita.IAttivitaService;
import org.entando.entando.plugins.jpaudit.aps.system.services.audit.IAuditService;
import org.entando.entando.plugins.jpaudit.aps.system.services.audit.model.AuditDto;
import org.entando.entando.plugins.jpaudit.web.audit.model.AuditRequest;
import org.entando.entando.plugins.jpsito.aps.system.services.sito.ISitoService;
import org.entando.entando.plugins.jpsito.aps.system.services.sito.model.SitoDto;
import org.entando.entando.plugins.jpsito.web.sito.model.SitoRequest;
import org.entando.entando.web.common.model.Filter;
import org.entando.entando.web.common.model.PagedMetadata;
import org.entando.entando.web.common.model.RestListRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.keybiz.aps.utils.Const;

@Service
public class CheckListServiceImpl implements CheckListService {

	@Autowired
	private ISitoService sitoService;

	@Autowired
	private IAttivitaService attivitaService;

	@Autowired
	private IAuditService auditService;

//	private static final String FILE_NAME = "C:\\Users\\pc_work\\Desktop\\import\\I373RM All 2 CHECK LIST DI VERIFICA_INWIT.XLSM";
//
//	public static void main(String[] args) throws FileNotFoundException {
//		CheckListServiceImpl p = new CheckListServiceImpl();
//		p.importData(new FileInputStream(new File(FILE_NAME)));
//	}

	@Override
	public void importData(InputStream excelFile) {
//		ApplicationContext context = null;
		Workbook workbook = null;
		try {
//			context = new ClassPathXmlApplicationContext(new String[] {"spring/plugins/jpattivita/aps/managers/jpattivitaAttivitaManagersConfig.xml",
//		              "spring/plugins/jpaudit/aps/managers/jpauditAuditManagersConfig.xml","spring/plugins/jpsito/aps/managers/jpsitoSitoManagersConfig.xml"});
			workbook = new XSSFWorkbook(excelFile);
			Sheet datatypeSheet = workbook.getSheetAt(0);
			DatiGenerali datiGenerali = this.initDatiGenerali(datatypeSheet);
			// cerca sito
			SitoDto sitoDto = null;
			RestListRequest requestSitos = new RestListRequest();
			requestSitos.setPageSize(null);
			PagedMetadata<SitoDto> responseSitos = sitoService.getSitos(requestSitos);
			for (SitoDto currentSitoDto : responseSitos.getBody()) {
				if (currentSitoDto.getCodiceSito().equalsIgnoreCase(datiGenerali.getCodice()))
					sitoDto = currentSitoDto;
			}
			if (sitoDto == null) {
				// crea sito
				SitoRequest sitoRequest = new SitoRequest();
				sitoRequest.setCodiceSito(datiGenerali.getCodice());
				sitoRequest.setDescrizioneSito(datiGenerali.getNomeSito());
				sitoRequest.setRegioneSito(datiGenerali.getRegione());
				sitoDto = sitoService.addSito(sitoRequest);
			}
			// cerca audit
			AuditDto auditDto = null;
			RestListRequest requestAudits = new RestListRequest();
			requestAudits.setPageSize(null);
			Filter dataAuditFilter = new Filter();
			dataAuditFilter.setAttribute("fkcodicesito");
			dataAuditFilter.setType("int");
			dataAuditFilter.setValue(Integer.toString(sitoDto.getId()));
			requestAudits.addFilter(dataAuditFilter);
			PagedMetadata<AuditDto> responseAudits = auditService.getAudits(requestAudits);
			for (AuditDto currentAuditDto : responseAudits.getBody()) {
				// verifica audit gia inserito per data
				if (currentAuditDto.getDataAudit().compareTo(datiGenerali.getDataAudit()) == 0) {
					// TODO notifica audit gia' presente
				}
			}
			if (auditDto == null) {
				// crea audit
				AuditRequest auditRequest = new AuditRequest();
				auditRequest.setDataAudit(datiGenerali.getDataAudit());
				auditRequest.setFkCodiceSito(Integer.toString(sitoDto.getId()));
				auditRequest.setIspettoreAudit(datiGenerali.getNominativoIspettore());
				auditRequest.setNoteGeneraliAudit(null);
				auditDto = auditService.addAudit(auditRequest);
				// crea attivita audit
				Map<Integer, String> intestazioneAttivita = new HashMap<Integer, String>();
				Iterator<Row> iterator = datatypeSheet.iterator();
				while (iterator.hasNext()) {
					Row currentRow = iterator.next();
					Iterator<Cell> cellIterator = currentRow.iterator();
					while (cellIterator.hasNext()) {
						Cell currentCell = cellIterator.next();
						Object cellValue = this.getCell(currentCell);
					}
					System.out.println();
				}
			}
			//
		} catch (FileNotFoundException e) {
			//TODO messaggio di errore
			e.printStackTrace();
		} catch (IOException e) {
			//TODO messaggio di errore
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				workbook.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	private Object getCell(Cell currentCell) {
		if (currentCell != null) {
			if (currentCell.getCellTypeEnum() == CellType.STRING) {
				return (currentCell.getStringCellValue() != null ? currentCell.getStringCellValue().trim() : null);
			} else if (currentCell.getCellTypeEnum() == CellType.NUMERIC) {
				if (DateUtil.isCellDateFormatted(currentCell)) {
					return currentCell.getDateCellValue();
				} else {
					return currentCell.getNumericCellValue();
				}
			} else if (currentCell.getCellTypeEnum() == CellType.BOOLEAN) {
				return currentCell.getBooleanCellValue();
			}
		}
		return null;
	}

	private DatiGenerali initDatiGenerali(Sheet datatypeSheet) {
		DatiGenerali datiGenerali = new DatiGenerali();
		//dati generali rows
		for (int i = 1; i <= 11; i++) {
			Row currentRow = datatypeSheet.getRow(i);
			Iterator<Cell> cellIterator = currentRow.iterator();
			stop: while (cellIterator.hasNext()) {
				Cell intestazione = cellIterator.next();
				if (intestazione.getCellTypeEnum() == CellType.STRING) {
					if (intestazione.getStringCellValue().toLowerCase().contains(Const.data_audit)) {
						datiGenerali.setDataAudit((Date) this.getCell(cellIterator.next()));
						break stop;
					} else if (intestazione.getStringCellValue().toLowerCase().contains(Const.nominativo_ispettore)) {
						datiGenerali.setNominativoIspettore((String) this.getCell(cellIterator.next()));
						break stop;
					} else if (intestazione.getStringCellValue().toLowerCase().contains(Const.zona_sito)) {
						datiGenerali.setZona((String) this.getCell(cellIterator.next()));
						break stop;
					} else if (intestazione.getStringCellValue().toLowerCase().contains(Const.regione_sito)) {
						datiGenerali.setRegione((String) this.getCell(cellIterator.next()));
						break stop;
					} else if (intestazione.getStringCellValue().toLowerCase().contains(Const.provincia_sito)) {
						datiGenerali.setProvincia((String) this.getCell(cellIterator.next()));
						break stop;
					} else if (intestazione.getStringCellValue().toLowerCase().contains(Const.codice_sito)) {
						datiGenerali.setCodice((String) this.getCell(cellIterator.next()));
						break stop;
					} else if (intestazione.getStringCellValue().toLowerCase().contains(Const.nome_sito)) {
						datiGenerali.setNomeSito((String) this.getCell(cellIterator.next()));
						break stop;
					}
				}
			}
		}
		return datiGenerali;
	}

	private class DatiGenerali {
		private Date dataAudit = null;
		private String nominativoIspettore = null;
		private String zona = null;
		private String regione = null;
		private String provincia = null;
		private String codice = null;
		private String nomeSito = null;

		public Date getDataAudit() {
			return dataAudit;
		}

		public void setDataAudit(Date dataAudit) {
			this.dataAudit = dataAudit;
		}

		public String getNominativoIspettore() {
			return nominativoIspettore;
		}

		public void setNominativoIspettore(String nominativoIspettore) {
			this.nominativoIspettore = nominativoIspettore;
		}

		public String getZona() {
			return zona;
		}

		public void setZona(String zona) {
			this.zona = zona;
		}

		public String getRegione() {
			return regione;
		}

		public void setRegione(String regione) {
			this.regione = regione;
		}

		public String getProvincia() {
			return provincia;
		}

		public void setProvincia(String provincia) {
			this.provincia = provincia;
		}

		public String getCodice() {
			return codice;
		}

		public void setCodice(String codice) {
			this.codice = codice;
		}

		public String getNomeSito() {
			return nomeSito;
		}

		public void setNomeSito(String nomeSito) {
			this.nomeSito = nomeSito;
		}
	}

}
