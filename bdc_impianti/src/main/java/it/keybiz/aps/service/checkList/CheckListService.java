package it.keybiz.aps.service.checkList;

import java.io.InputStream;

public interface CheckListService {

	void importData(InputStream excelFile);

}
