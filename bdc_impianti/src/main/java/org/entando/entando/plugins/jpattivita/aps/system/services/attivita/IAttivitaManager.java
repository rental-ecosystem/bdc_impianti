/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpattivita.aps.system.services.attivita;

import java.util.List;
import com.agiletec.aps.system.exception.ApsSystemException;
import com.agiletec.aps.system.common.model.dao.SearcherDaoPaginatedResult;

import com.agiletec.aps.system.common.FieldSearchFilter;

public interface IAttivitaManager {

	public Attivita getAttivita(int id) throws ApsSystemException;

	public List<Integer> getAttivitas() throws ApsSystemException;

	public List<Integer> searchAttivitas(FieldSearchFilter filters[]) throws ApsSystemException;

	public void addAttivita(Attivita attivita) throws ApsSystemException;

	public void updateAttivita(Attivita attivita) throws ApsSystemException;

	public void deleteAttivita(int id) throws ApsSystemException;

	public SearcherDaoPaginatedResult<Attivita> getAttivitas(List<FieldSearchFilter> fieldSearchFilters) throws ApsSystemException;

	void addAttivitaList(List<Attivita> attivitaList) throws ApsSystemException;
}