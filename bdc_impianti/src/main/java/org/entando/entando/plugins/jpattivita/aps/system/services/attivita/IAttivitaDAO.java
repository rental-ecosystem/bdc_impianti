/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpattivita.aps.system.services.attivita;

import java.util.List;

import com.agiletec.aps.system.common.FieldSearchFilter;

public interface IAttivitaDAO {

	public List<Integer> searchAttivitas(FieldSearchFilter[] filters);
	
	public Attivita loadAttivita(int id);

	public List<Integer> loadAttivitas();

	public void removeAttivita(int id);
	
	public void updateAttivita(Attivita attivita);

	public void insertAttivita(Attivita attivita);

    public int countAttivitas(FieldSearchFilter[] filters);

	void insertAttivitaList(List<Attivita> attivitaList);
}