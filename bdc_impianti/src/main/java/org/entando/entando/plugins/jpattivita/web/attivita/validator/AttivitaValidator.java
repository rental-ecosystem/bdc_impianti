/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpattivita.web.attivita.validator;

import org.apache.commons.lang3.StringUtils;
import org.entando.entando.web.common.validator.AbstractPaginationValidator;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.entando.entando.plugins.jpattivita.web.attivita.model.AttivitaRequest;


@Component
public class AttivitaValidator extends AbstractPaginationValidator {

    public static final String ERRCODE_URINAME_MISMATCH = "1";
    public static final String ERRCODE_ATTIVITA_NOT_FOUND = "2" ;
    public static final String ERRCODE_ATTIVITA_ALREADY_EXISTS = "3";


    @Override
    public boolean supports(Class<?> paramClass) {
        return AttivitaRequest.class.equals(paramClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        //AttivitaRequest request = (AttivitaRequest) target;
    }

    public void validateBodyName(String attivitaId, AttivitaRequest attivitaRequest, Errors errors) {
        if (!StringUtils.equals(attivitaId, String.valueOf(attivitaRequest.getId_att()))) {
            errors.rejectValue("id", ERRCODE_URINAME_MISMATCH, new Object[]{attivitaId, attivitaRequest.getId_att()}, "attivita.id.mismatch");
        }
    }

    @Override
    protected String getDefaultSortProperty() {
        return "id_att";
    }

}
