/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpsito.web.sito;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import com.agiletec.aps.system.services.role.Permission;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.entando.entando.web.common.annotation.RestAccessControl;
import org.entando.entando.web.common.exceptions.ValidationConflictException;
import org.entando.entando.web.common.exceptions.ValidationGenericException;
import org.entando.entando.web.common.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.entando.entando.plugins.jpsito.aps.system.services.sito.ISitoService;
import org.entando.entando.plugins.jpsito.aps.system.services.sito.model.SitoDto;
import org.entando.entando.plugins.jpsito.web.sito.model.SitoRequest;
import org.entando.entando.plugins.jpsito.web.sito.validator.SitoValidator;

@RestController
@RequestMapping(value = "/jpsito/sitos")
public class SitoController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ISitoService sitoService;

    @Autowired
    private SitoValidator sitoValidator;

    protected ISitoService getSitoService() {
        return sitoService;
    }

    public void setSitoService(ISitoService sitoService) {
        this.sitoService = sitoService;
    }

    protected SitoValidator getSitoValidator() {
        return sitoValidator;
    }

    public void setSitoValidator(SitoValidator sitoValidator) {
        this.sitoValidator = sitoValidator;
    }

    @RestAccessControl(permission = "superuser")
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PagedRestResponse<SitoDto>> getSitos(RestListRequest requestList) throws JsonProcessingException {
        this.getSitoValidator().validateRestListRequest(requestList, SitoDto.class);
        PagedMetadata<SitoDto> result = this.getSitoService().getSitos(requestList);
        this.getSitoValidator().validateRestListResult(requestList, result);
        logger.debug("Main Response -> {}", result);
        return new ResponseEntity<>(new PagedRestResponse<>(result), HttpStatus.OK);
    }

    @RestAccessControl(permission = "superuser")
    @RequestMapping(value = "/{sitoId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SimpleRestResponse<SitoDto>> getSito(@PathVariable String sitoId) {
		SitoDto sito = this.getSitoService().getSito(Integer.valueOf(sitoId));
        return new ResponseEntity<>(new SimpleRestResponse<>(sito), HttpStatus.OK);
    }

    @RestAccessControl(permission = "superuser")
    @RequestMapping(value = "/{sitoId}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SimpleRestResponse<SitoDto>> updateSito(@PathVariable String sitoId, @Valid @RequestBody SitoRequest sitoRequest, BindingResult bindingResult) {
        //field validations
        if (bindingResult.hasErrors()) {
            throw new ValidationGenericException(bindingResult);
        }
        this.getSitoValidator().validateBodyName(String.valueOf(sitoId), sitoRequest, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new ValidationGenericException(bindingResult);
        }

        SitoDto sito = this.getSitoService().updateSito(sitoRequest);
        return new ResponseEntity<>(new SimpleRestResponse<>(sito), HttpStatus.OK);
    }

    @RestAccessControl(permission = "superuser")
    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SimpleRestResponse<SitoDto>> addSito(@Valid @RequestBody SitoRequest sitoRequest, BindingResult bindingResult) {
        //field validations
        if (bindingResult.hasErrors()) {
            throw new ValidationGenericException(bindingResult);
        }
        //business validations
        getSitoValidator().validate(sitoRequest, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new ValidationConflictException(bindingResult);
        }
        SitoDto dto = this.getSitoService().addSito(sitoRequest);
        return new ResponseEntity<>(new SimpleRestResponse<>(dto), HttpStatus.OK);
    }

    @RestAccessControl(permission = "superuser")
    @RequestMapping(value = "/{sitoId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SimpleRestResponse<Map>> deleteSito(@PathVariable String sitoId) {
        logger.info("deleting {}", sitoId);
        this.getSitoService().removeSito(Integer.valueOf(sitoId));
        Map<String, Integer> result = new HashMap<>();
        result.put("id", Integer.valueOf(sitoId));
        return new ResponseEntity<>(new SimpleRestResponse<>(result), HttpStatus.OK);
    }

}

