/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpaudit.aps.system.services.audit;

import java.util.List;
import com.agiletec.aps.system.exception.ApsSystemException;
import com.agiletec.aps.system.common.model.dao.SearcherDaoPaginatedResult;
import java.util.Date;
import com.agiletec.aps.system.common.FieldSearchFilter;

public interface IAuditManager {

	public Audit getAudit(int id) throws ApsSystemException;

	public List<Integer> getAudits() throws ApsSystemException;

	public List<Integer> searchAudits(FieldSearchFilter filters[]) throws ApsSystemException;

	public void addAudit(Audit audit) throws ApsSystemException;

	public void updateAudit(Audit audit) throws ApsSystemException;

	public void deleteAudit(int id) throws ApsSystemException;

	public SearcherDaoPaginatedResult<Audit> getAudits(List<FieldSearchFilter> fieldSearchFilters) throws ApsSystemException;
}