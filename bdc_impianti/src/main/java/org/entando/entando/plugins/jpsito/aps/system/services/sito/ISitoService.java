/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpsito.aps.system.services.sito;

import  org.entando.entando.plugins.jpsito.aps.system.services.sito.model.SitoDto;
import org.entando.entando.web.common.model.PagedMetadata;
import org.entando.entando.web.common.model.RestListRequest;
import org.entando.entando.plugins.jpsito.web.sito.model.SitoRequest;

public interface ISitoService {

    public String BEAN_NAME = "jpsitoSitoService";

    public PagedMetadata<SitoDto> getSitos(RestListRequest requestList);

    public SitoDto updateSito(SitoRequest sitoRequest);

    public SitoDto addSito(SitoRequest sitoRequest);

    public void removeSito(int id);

    public SitoDto getSito(int  id);

}

