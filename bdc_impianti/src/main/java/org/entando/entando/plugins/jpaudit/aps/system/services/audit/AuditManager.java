/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpaudit.aps.system.services.audit;

import org.entando.entando.plugins.jpaudit.aps.system.services.audit.event.AuditChangedEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Date;
import com.agiletec.aps.system.common.FieldSearchFilter;
import com.agiletec.aps.system.common.AbstractService;
import com.agiletec.aps.system.exception.ApsSystemException;
import com.agiletec.aps.system.services.keygenerator.IKeyGeneratorManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.agiletec.aps.system.common.model.dao.SearcherDaoPaginatedResult;

public class AuditManager extends AbstractService implements IAuditManager {

	private static final Logger logger =  LoggerFactory.getLogger(AuditManager.class);

	@Override
	public void init() throws Exception {
		logger.debug("{} ready.", this.getClass().getName());
	}
 
	@Override
	public Audit getAudit(int id) throws ApsSystemException {
		Audit audit = null;
		try {
			audit = this.getAuditDAO().loadAudit(id);
		} catch (Throwable t) {
			logger.error("Error loading audit with id '{}'", id,  t);
			throw new ApsSystemException("Error loading audit with id: " + id, t);
		}
		return audit;
	}

	@Override
	public List<Integer> getAudits() throws ApsSystemException {
		List<Integer> audits = new ArrayList<Integer>();
		try {
			audits = this.getAuditDAO().loadAudits();
		} catch (Throwable t) {
			logger.error("Error loading Audit list",  t);
			throw new ApsSystemException("Error loading Audit ", t);
		}
		return audits;
	}

	@Override
	public List<Integer> searchAudits(FieldSearchFilter filters[]) throws ApsSystemException {
		List<Integer> audits = new ArrayList<Integer>();
		try {
			audits = this.getAuditDAO().searchAudits(filters);
		} catch (Throwable t) {
			logger.error("Error searching Audits", t);
			throw new ApsSystemException("Error searching Audits", t);
		}
		return audits;
	}

	@Override
	public void addAudit(Audit audit) throws ApsSystemException {
		try {
			this.getAuditDAO().insertAudit(audit);
			this.notifyAuditChangedEvent(audit, AuditChangedEvent.INSERT_OPERATION_CODE);
		} catch (Throwable t) {
			logger.error("Error adding Audit", t);
			throw new ApsSystemException("Error adding Audit", t);
		}
	}
 
	@Override
	public void updateAudit(Audit audit) throws ApsSystemException {
		try {
			this.getAuditDAO().updateAudit(audit);
			this.notifyAuditChangedEvent(audit, AuditChangedEvent.UPDATE_OPERATION_CODE);
		} catch (Throwable t) {
			logger.error("Error updating Audit", t);
			throw new ApsSystemException("Error updating Audit " + audit, t);
		}
	}

	@Override
	public void deleteAudit(int id) throws ApsSystemException {
		try {
			Audit audit = this.getAudit(id);
			this.getAuditDAO().removeAudit(id);
			this.notifyAuditChangedEvent(audit, AuditChangedEvent.REMOVE_OPERATION_CODE);
		} catch (Throwable t) {
			logger.error("Error deleting Audit with id {}", id, t);
			throw new ApsSystemException("Error deleting Audit with id:" + id, t);
		}
	}


	private void notifyAuditChangedEvent(Audit audit, int operationCode) {
		AuditChangedEvent event = new AuditChangedEvent();
		event.setAudit(audit);
		event.setOperationCode(operationCode);
		this.notifyEvent(event);
	}

    @SuppressWarnings("rawtypes")
    public SearcherDaoPaginatedResult<Audit> getAudits(FieldSearchFilter[] filters) throws ApsSystemException {
        SearcherDaoPaginatedResult<Audit> pagedResult = null;
        try {
            List<Audit> audits = new ArrayList<>();
            int count = this.getAuditDAO().countAudits(filters);

            List<Integer> auditNames = this.getAuditDAO().searchAudits(filters);
            for (Integer auditName : auditNames) {
                audits.add(this.getAudit(auditName));
            }
            pagedResult = new SearcherDaoPaginatedResult<Audit>(count, audits);
        } catch (Throwable t) {
            logger.error("Error searching audits", t);
            throw new ApsSystemException("Error searching audits", t);
        }
        return pagedResult;
    }

    @Override
    public SearcherDaoPaginatedResult<Audit> getAudits(List<FieldSearchFilter> filters) throws ApsSystemException {
        FieldSearchFilter[] array = null;
        if (null != filters) {
            array = filters.toArray(new FieldSearchFilter[filters.size()]);
        }
        return this.getAudits(array);
    }


	protected IKeyGeneratorManager getKeyGeneratorManager() {
		return _keyGeneratorManager;
	}
	public void setKeyGeneratorManager(IKeyGeneratorManager keyGeneratorManager) {
		this._keyGeneratorManager = keyGeneratorManager;
	}

	public void setAuditDAO(IAuditDAO auditDAO) {
		 this._auditDAO = auditDAO;
	}
	protected IAuditDAO getAuditDAO() {
		return _auditDAO;
	}

	private IKeyGeneratorManager _keyGeneratorManager;
	private IAuditDAO _auditDAO;
}
