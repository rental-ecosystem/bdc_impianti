/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpattivita.aps.system.services.attivita;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.agiletec.aps.system.common.AbstractSearcherDAO;
import com.agiletec.aps.system.common.FieldSearchFilter;

public class AttivitaDAO extends AbstractSearcherDAO implements IAttivitaDAO {

	private static final Logger logger =  LoggerFactory.getLogger(AttivitaDAO.class);

    @Override
    public int countAttivitas(FieldSearchFilter[] filters) {
        Integer attivitas = null;
        try {
            attivitas = super.countId(filters);
        } catch (Throwable t) {
            logger.error("error in count attivitas", t);
            throw new RuntimeException("error in count attivitas", t);
        }
        return attivitas;
    }

	@Override
	protected String getTableFieldName(String metadataFieldKey) {
		return metadataFieldKey;
	}
	
	@Override
	protected String getMasterTableName() {
		return "attivita";
	}
	
	@Override
	protected String getMasterTableIdFieldName() {
		return "id";
	}

    @Override
    public List<Integer> searchAttivitas(FieldSearchFilter[] filters) {
            List<Integer> attivitasId = new ArrayList<>();
        List<String> masterList = super.searchId(filters);
        masterList.stream().forEach(idString -> attivitasId.add(Integer.parseInt(idString)));
        return attivitasId;
        }


	@Override
	public List<Integer> loadAttivitas() {
		List<Integer> attivitasId = new ArrayList<Integer>();
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet res = null;
		try {
			conn = this.getConnection();
			stat = conn.prepareStatement(LOAD_ATTIVITAS_ID);
			res = stat.executeQuery();
			while (res.next()) {
				int id = res.getInt("id");
				attivitasId.add(id);
			}
		} catch (Throwable t) {
			logger.error("Error loading Attivita list",  t);
			throw new RuntimeException("Error loading Attivita list", t);
		} finally {
			closeDaoResources(res, stat, conn);
		}
		return attivitasId;
	}
	
	@Override
	public void insertAttivita(Attivita attivita) {
		PreparedStatement stat = null;
		Connection conn  = null;
		try {
			conn = this.getConnection();
			conn.setAutoCommit(false);
			this.insertAttivita(attivita, conn);
 			conn.commit();
		} catch (Throwable t) {
			this.executeRollback(conn);
			logger.error("Error on insert attivita",  t);
			throw new RuntimeException("Error on insert attivita", t);
		} finally {
			this.closeDaoResources(null, stat, conn);
		}
	}
	
	public void insertAttivitaList(List<Attivita> attivitaList) {
		PreparedStatement stat = null;
		Connection conn  = null;
		try {
			conn = this.getConnection();
			conn.setAutoCommit(false);
			for(Attivita attivita : attivitaList) {
				this.insertAttivita(attivita, conn);				
			}
 			conn.commit();
		} catch (Throwable t) {
			this.executeRollback(conn);
			logger.error("Error on insert attivita",  t);
			throw new RuntimeException("Error on insert attivita", t);
		} finally {
			this.closeDaoResources(null, stat, conn);
		}
	}

	public void insertAttivita(Attivita attivita, Connection conn) {
		PreparedStatement stat = null;
		try {
			stat = conn.prepareStatement(ADD_ATTIVITA);
			int index = 1;
			stat.setInt(index++, attivita.getId_att());
 			stat.setString(index++, attivita.getCodiceRiferimentoAtt());
 			stat.setString(index++, attivita.getDescrizioneAtt());
 			stat.setString(index++, attivita.getDescrizioneMacroAtt());
			stat.setInt(index++, attivita.getImpattoAttivita());
 			stat.setString(index++, attivita.getNoteAttivita());
			stat.setInt(index++, attivita.getFkValutazioneAtt());
			stat.setInt(index++, attivita.getFkAuditAtt());
			stat.executeUpdate();
		} catch (Throwable t) {
			logger.error("Error on insert attivita",  t);
			throw new RuntimeException("Error on insert attivita", t);
		} finally {
			this.closeDaoResources(null, stat, null);
		}
	}

	@Override
	public void updateAttivita(Attivita attivita) {
		PreparedStatement stat = null;
		Connection conn = null;
		try {
			conn = this.getConnection();
			conn.setAutoCommit(false);
			this.updateAttivita(attivita, conn);
 			conn.commit();
		} catch (Throwable t) {
			this.executeRollback(conn);
			logger.error("Error updating attivita {}", attivita.getId_att(),  t);
			throw new RuntimeException("Error updating attivita", t);
		} finally {
			this.closeDaoResources(null, stat, conn);
		}
	}

	public void updateAttivita(Attivita attivita, Connection conn) {
		PreparedStatement stat = null;
		try {
			stat = conn.prepareStatement(UPDATE_ATTIVITA);
			int index = 1;

			stat.setInt(index++, attivita.getId_att());
 			stat.setString(index++, attivita.getCodiceRiferimentoAtt());
 			stat.setString(index++, attivita.getDescrizioneAtt());
 			stat.setString(index++, attivita.getDescrizioneMacroAtt());
			stat.setInt(index++, attivita.getImpattoAttivita());
 			stat.setString(index++, attivita.getNoteAttivita());
			stat.setInt(index++, attivita.getFkValutazioneAtt());
			stat.setInt(index++, attivita.getFkAuditAtt());
			stat.executeUpdate();
		} catch (Throwable t) {
			logger.error("Error updating attivita {}", attivita.getId_att(),  t);
			throw new RuntimeException("Error updating attivita", t);
		} finally {
			this.closeDaoResources(null, stat, null);
		}
	}

	@Override
	public void removeAttivita(int id) {
		PreparedStatement stat = null;
		Connection conn = null;
		try {
			conn = this.getConnection();
			conn.setAutoCommit(false);
			this.removeAttivita(id, conn);
 			conn.commit();
		} catch (Throwable t) {
			this.executeRollback(conn);
			logger.error("Error deleting attivita {}", id, t);
			throw new RuntimeException("Error deleting attivita", t);
		} finally {
			this.closeDaoResources(null, stat, conn);
		}
	}
	
	public void removeAttivita(int id, Connection conn) {
		PreparedStatement stat = null;
		try {
			stat = conn.prepareStatement(DELETE_ATTIVITA);
			int index = 1;
			stat.setInt(index++, id);
			stat.executeUpdate();
		} catch (Throwable t) {
			logger.error("Error deleting attivita {}", id, t);
			throw new RuntimeException("Error deleting attivita", t);
		} finally {
			this.closeDaoResources(null, stat, null);
		}
	}

	public Attivita loadAttivita(int id) {
		Attivita attivita = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet res = null;
		try {
			conn = this.getConnection();
			attivita = this.loadAttivita(id, conn);
		} catch (Throwable t) {
			logger.error("Error loading attivita with id {}", id, t);
			throw new RuntimeException("Error loading attivita with id " + id, t);
		} finally {
			closeDaoResources(res, stat, conn);
		}
		return attivita;
	}

	public Attivita loadAttivita(int id, Connection conn) {
		Attivita attivita = null;
		PreparedStatement stat = null;
		ResultSet res = null;
		try {
			stat = conn.prepareStatement(LOAD_ATTIVITA);
			int index = 1;
			stat.setInt(index++, id);
			res = stat.executeQuery();
			if (res.next()) {
				attivita = this.buildAttivitaFromRes(res);
			}
		} catch (Throwable t) {
			logger.error("Error loading attivita with id {}", id, t);
			throw new RuntimeException("Error loading attivita with id " + id, t);
		} finally {
			closeDaoResources(res, stat, null);
		}
		return attivita;
	}

	protected Attivita buildAttivitaFromRes(ResultSet res) {
		Attivita attivita = null;
		try {
			attivita = new Attivita();				
			attivita.setId_att(res.getInt("id_att"));
			attivita.setCodiceRiferimentoAtt(res.getString("codicerifatt"));
			attivita.setDescrizioneAtt(res.getString("descrizioneatt"));
			attivita.setDescrizioneMacroAtt(res.getString("descrizionemacroatt"));
			attivita.setImpattoAttivita(res.getInt("impattoattivita"));
			attivita.setNoteAttivita(res.getString("noteattivita"));
			attivita.setFkValutazioneAtt(res.getInt("fkvalutazioneatt"));
			attivita.setFkAuditAtt(res.getInt("fkauditatt"));
		} catch (Throwable t) {
			logger.error("Error in buildAttivitaFromRes", t);
		}
		return attivita;
	}

	private static final String ADD_ATTIVITA = "INSERT INTO attivita (id_att, codicerifatt, descrizioneatt, descrizionemacroatt, impattoattivita, noteattivita, fkvalutazioneatt, fkauditatt ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ? )";

	private static final String UPDATE_ATTIVITA = "UPDATE attivita SET  id_att=?,  codicerifatt=?,  descrizioneatt=?,  descrizionemacroatt=?,  impattoattivita=?,  noteattivita=?,  fkvalutazioneatt=?, fkauditatt=? WHERE id_att = ?";

	private static final String DELETE_ATTIVITA = "DELETE FROM attivita WHERE id_att = ?";
	
	private static final String LOAD_ATTIVITA = "SELECT id_att, codicerifatt, descrizioneatt, descrizionemacroatt, impattoattivita, noteattivita, fkvalutazioneatt, fkauditatt  FROM attivita WHERE id_att = ?";
	
	private static final String LOAD_ATTIVITAS_ID  = "SELECT id_att FROM attivita";
	
}