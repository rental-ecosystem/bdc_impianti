/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpattivita.aps.system.services.attivita.event;

import com.agiletec.aps.system.common.notify.ObserverService;

public interface AttivitaChangedObserver extends ObserverService {
	
	public void updateFromAttivitaChanged(AttivitaChangedEvent event);
	
}
