/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpsito.aps.system.services.sito.event;

import com.agiletec.aps.system.common.IManager;
import com.agiletec.aps.system.common.notify.ApsEvent;
import org.entando.entando.plugins.jpsito.aps.system.services.sito.Sito;


public class SitoChangedEvent extends ApsEvent {
	
	@Override
	public void notify(IManager srv) {
		((SitoChangedObserver) srv).updateFromSitoChanged(this);
	}
	
	@Override
	public Class getObserverInterface() {
		return SitoChangedObserver.class;
	}
	
	public int getOperationCode() {
		return _operationCode;
	}
	public void setOperationCode(int operationCode) {
		this._operationCode = operationCode;
	}
	
	public Sito getSito() {
		return _sito;
	}
	public void setSito(Sito sito) {
		this._sito = sito;
	}

	private Sito _sito;
	private int _operationCode;
	
	public static final int INSERT_OPERATION_CODE = 1;
	public static final int REMOVE_OPERATION_CODE = 2;
	public static final int UPDATE_OPERATION_CODE = 3;

}
