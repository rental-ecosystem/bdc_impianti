/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpaudit.aps.system.services.audit;

import  org.entando.entando.plugins.jpaudit.aps.system.services.audit.model.AuditDto;
import org.entando.entando.web.common.model.PagedMetadata;
import org.entando.entando.web.common.model.RestListRequest;
import org.entando.entando.plugins.jpaudit.web.audit.model.AuditRequest;

public interface IAuditService {

    public String BEAN_NAME = "jpauditAuditService";

    public PagedMetadata<AuditDto> getAudits(RestListRequest requestList);

    public AuditDto updateAudit(AuditRequest auditRequest);

    public AuditDto addAudit(AuditRequest auditRequest);

    public void removeAudit(int id);

    public AuditDto getAudit(int  id);

}

