/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpaudit.aps.system.services.audit;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.sql.Timestamp;
import java.util.Date;
import org.apache.commons.lang.StringUtils;
import com.agiletec.aps.system.common.AbstractSearcherDAO;
import com.agiletec.aps.system.common.FieldSearchFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class AuditDAO extends AbstractSearcherDAO implements IAuditDAO {

	private static final Logger logger =  LoggerFactory.getLogger(AuditDAO.class);

    @Override
    public int countAudits(FieldSearchFilter[] filters) {
        Integer audits = null;
        try {
            audits = super.countId(filters);
        } catch (Throwable t) {
            logger.error("error in count audits", t);
            throw new RuntimeException("error in count audits", t);
        }
        return audits;
    }

	@Override
	protected String getTableFieldName(String metadataFieldKey) {
		return metadataFieldKey;
	}
	
	@Override
	protected String getMasterTableName() {
		return "auditsito";
	}
	
	@Override
	protected String getMasterTableIdFieldName() {
		return "id_audit";
	}

    @Override
    public List<Integer> searchAudits(FieldSearchFilter[] filters) {
            List<Integer> auditsId = new ArrayList<>();
        List<String> masterList = super.searchId(filters);
        masterList.stream().forEach(idString -> auditsId.add(Integer.parseInt(idString)));
        return auditsId;
        }


	@Override
	public List<Integer> loadAudits() {
		List<Integer> auditsId = new ArrayList<Integer>();
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet res = null;
		try {
			conn = this.getConnection();
			stat = conn.prepareStatement(LOAD_AUDITS_ID);
			res = stat.executeQuery();
			while (res.next()) {
				int id = res.getInt("id");
				auditsId.add(id);
			}
		} catch (Throwable t) {
			logger.error("Error loading Audit list",  t);
			throw new RuntimeException("Error loading Audit list", t);
		} finally {
			closeDaoResources(res, stat, conn);
		}
		return auditsId;
	}
	
	@Override
	public void insertAudit(Audit audit) {
		PreparedStatement stat = null;
		Connection conn  = null;
		try {
			conn = this.getConnection();
			conn.setAutoCommit(false);
			this.insertAudit(audit, conn);
 			conn.commit();
		} catch (Throwable t) {
			this.executeRollback(conn);
			logger.error("Error on insert audit",  t);
			throw new RuntimeException("Error on insert audit", t);
		} finally {
			this.closeDaoResources(null, stat, conn);
		}
	}

	public void insertAudit(Audit audit, Connection conn) {
		PreparedStatement stat = null;
		try {
			stat = conn.prepareStatement(ADD_AUDIT);
			int index = 1;
			stat.setInt(index++, audit.getIdAudit());
			stat.setInt(index++, audit.getValoreVerificaAudit());
 			stat.setString(index++, audit.getIspettoreAudit());
 			stat.setString(index++, audit.getNoteGeneraliAudit());
			Timestamp dataAuditTimestamp = new Timestamp(audit.getDataAudit().getTime());
			stat.setTimestamp(index++, dataAuditTimestamp);	
 			Timestamp dataInserimentoAuditTimestamp = new Timestamp(audit.getDataInserimentoAudit().getTime());
			stat.setTimestamp(index++, dataInserimentoAuditTimestamp);	
 			Timestamp dataModificaAuditTimestamp = new Timestamp(audit.getDataModificaAudit().getTime());
			stat.setTimestamp(index++, dataModificaAuditTimestamp);	
  			stat.setString(index++, audit.getFkCodiceSito());
			stat.executeUpdate();
		} catch (Throwable t) {
			logger.error("Error on insert audit",  t);
			throw new RuntimeException("Error on insert audit", t);
		} finally {
			this.closeDaoResources(null, stat, null);
		}
	}

	@Override
	public void updateAudit(Audit audit) {
		PreparedStatement stat = null;
		Connection conn = null;
		try {
			conn = this.getConnection();
			conn.setAutoCommit(false);
			this.updateAudit(audit, conn);
 			conn.commit();
		} catch (Throwable t) {
			this.executeRollback(conn);
			logger.error("Error updating audit {}", audit.getIdAudit(),  t);
			throw new RuntimeException("Error updating audit", t);
		} finally {
			this.closeDaoResources(null, stat, conn);
		}
	}

	public void updateAudit(Audit audit, Connection conn) {
		PreparedStatement stat = null;
		try {
			stat = conn.prepareStatement(UPDATE_AUDIT);
			int index = 1;

			stat.setInt(index++, audit.getIdAudit());
			stat.setInt(index++, audit.getValoreVerificaAudit());
 			stat.setString(index++, audit.getIspettoreAudit());
 			stat.setString(index++, audit.getNoteGeneraliAudit());
			Timestamp dataAuditTimestamp = new Timestamp(audit.getDataAudit().getTime());
			stat.setTimestamp(index++, dataAuditTimestamp);	
 			Timestamp dataInserimentoAuditTimestamp = new Timestamp(audit.getDataInserimentoAudit().getTime());
			stat.setTimestamp(index++, dataInserimentoAuditTimestamp);	
 			Timestamp dataModificaAuditTimestamp = new Timestamp(audit.getDataModificaAudit().getTime());
			stat.setTimestamp(index++, dataModificaAuditTimestamp);	
  			stat.setString(index++, audit.getFkCodiceSito());
			stat.setInt(index++, audit.getIdAudit());
			stat.executeUpdate();
		} catch (Throwable t) {
			logger.error("Error updating audit {}", audit.getIdAudit(),  t);
			throw new RuntimeException("Error updating audit", t);
		} finally {
			this.closeDaoResources(null, stat, null);
		}
	}

	@Override
	public void removeAudit(int id) {
		PreparedStatement stat = null;
		Connection conn = null;
		try {
			conn = this.getConnection();
			conn.setAutoCommit(false);
			this.removeAudit(id, conn);
 			conn.commit();
		} catch (Throwable t) {
			this.executeRollback(conn);
			logger.error("Error deleting audit {}", id, t);
			throw new RuntimeException("Error deleting audit", t);
		} finally {
			this.closeDaoResources(null, stat, conn);
		}
	}
	
	public void removeAudit(int id, Connection conn) {
		PreparedStatement stat = null;
		try {
			stat = conn.prepareStatement(DELETE_AUDIT);
			int index = 1;
			stat.setInt(index++, id);
			stat.executeUpdate();
		} catch (Throwable t) {
			logger.error("Error deleting audit {}", id, t);
			throw new RuntimeException("Error deleting audit", t);
		} finally {
			this.closeDaoResources(null, stat, null);
		}
	}

	public Audit loadAudit(int id) {
		Audit audit = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet res = null;
		try {
			conn = this.getConnection();
			audit = this.loadAudit(id, conn);
		} catch (Throwable t) {
			logger.error("Error loading audit with id {}", id, t);
			throw new RuntimeException("Error loading audit with id " + id, t);
		} finally {
			closeDaoResources(res, stat, conn);
		}
		return audit;
	}

	public Audit loadAudit(int id, Connection conn) {
		Audit audit = null;
		PreparedStatement stat = null;
		ResultSet res = null;
		try {
			stat = conn.prepareStatement(LOAD_AUDIT);
			int index = 1;
			stat.setInt(index++, id);
			res = stat.executeQuery();
			if (res.next()) {
				audit = this.buildAuditFromRes(res);
			}
		} catch (Throwable t) {
			logger.error("Error loading audit with id {}", id, t);
			throw new RuntimeException("Error loading audit with id " + id, t);
		} finally {
			closeDaoResources(res, stat, null);
		}
		return audit;
	}

	protected Audit buildAuditFromRes(ResultSet res) {
		Audit audit = null;
		try {
			audit = new Audit();				
			audit.setIdAudit(res.getInt("id_audit"));
			audit.setValoreVerificaAudit(res.getInt("valoreverificaaudit"));
			audit.setIspettoreAudit(res.getString("ispettoreaudit"));
			audit.setNoteGeneraliAudit(res.getString("notegeneraliaudit"));
			Timestamp dataAuditValue = res.getTimestamp("dataaudit");
			if (null != dataAuditValue) {
				audit.setDataAudit(new Date(dataAuditValue.getTime()));
			}
			Timestamp dataInserimentoAuditValue = res.getTimestamp("datainserimentoaudit");
			if (null != dataInserimentoAuditValue) {
				audit.setDataInserimentoAudit(new Date(dataInserimentoAuditValue.getTime()));
			}
			Timestamp dataModificaAuditValue = res.getTimestamp("datamodificaaudit");
			if (null != dataModificaAuditValue) {
				audit.setDataModificaAudit(new Date(dataModificaAuditValue.getTime()));
			}
			audit.setFkCodiceSito(res.getString("fksito"));
		} catch (Throwable t) {
			logger.error("Error in buildAuditFromRes", t);
		}
		return audit;
	}

	private static final String ADD_AUDIT = "INSERT INTO auditsito (id_audit, valoreverificaaudit, ispettoreaudit, notegeneraliaudit, dataaudit, datainserimentoaudit, datamodificaaudit, fksito ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ? )";

	private static final String UPDATE_AUDIT = "UPDATE auditsito SET  id_audit=?,  valoreverificaaudit=?,  ispettoreaudit=?,  notegeneraliaudit=?,  dataaudit=?,  datainserimentoaudit=?,  datamodificaaudit=?, fksito=? WHERE id_audit = ?";

	private static final String DELETE_AUDIT = "DELETE FROM auditsito WHERE id_audit = ?";
	
	private static final String LOAD_AUDIT = "SELECT id_audit, valoreverificaaudit, ispettoreaudit, notegeneraliaudit, dataaudit, datainserimentoaudit, datamodificaaudit, fksito  FROM auditsito WHERE id_audit = ?";
	
	private static final String LOAD_AUDITS_ID  = "SELECT id_audit FROM auditsito";
	
}