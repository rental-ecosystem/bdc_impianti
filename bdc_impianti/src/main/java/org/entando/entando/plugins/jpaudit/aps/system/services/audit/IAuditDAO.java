/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpaudit.aps.system.services.audit;

import java.util.List;
import java.util.Date;
import com.agiletec.aps.system.common.FieldSearchFilter;

public interface IAuditDAO {

	public List<Integer> searchAudits(FieldSearchFilter[] filters);
	
	public Audit loadAudit(int id);

	public List<Integer> loadAudits();

	public void removeAudit(int id);
	
	public void updateAudit(Audit audit);

	public void insertAudit(Audit audit);

    public int countAudits(FieldSearchFilter[] filters);
}