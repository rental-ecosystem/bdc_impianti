/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpattivita.aps.system.services.attivita.event;

import com.agiletec.aps.system.common.IManager;
import com.agiletec.aps.system.common.notify.ApsEvent;
import org.entando.entando.plugins.jpattivita.aps.system.services.attivita.Attivita;


public class AttivitaChangedEvent extends ApsEvent {
	
	@Override
	public void notify(IManager srv) {
		((AttivitaChangedObserver) srv).updateFromAttivitaChanged(this);
	}
	
	@Override
	public Class getObserverInterface() {
		return AttivitaChangedObserver.class;
	}
	
	public int getOperationCode() {
		return _operationCode;
	}
	public void setOperationCode(int operationCode) {
		this._operationCode = operationCode;
	}
	
	public Attivita getAttivita() {
		return _attivita;
	}
	public void setAttivita(Attivita attivita) {
		this._attivita = attivita;
	}

	private Attivita _attivita;
	private int _operationCode;
	
	public static final int INSERT_OPERATION_CODE = 1;
	public static final int REMOVE_OPERATION_CODE = 2;
	public static final int UPDATE_OPERATION_CODE = 3;

}
