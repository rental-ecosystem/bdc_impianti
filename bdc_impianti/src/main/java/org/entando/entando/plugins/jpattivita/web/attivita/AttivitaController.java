/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpattivita.web.attivita;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import com.agiletec.aps.system.services.role.Permission;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.entando.entando.web.common.annotation.RestAccessControl;
import org.entando.entando.web.common.exceptions.ValidationConflictException;
import org.entando.entando.web.common.exceptions.ValidationGenericException;
import org.entando.entando.web.common.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.entando.entando.plugins.jpattivita.aps.system.services.attivita.IAttivitaService;
import org.entando.entando.plugins.jpattivita.aps.system.services.attivita.model.AttivitaDto;
import org.entando.entando.plugins.jpattivita.web.attivita.model.AttivitaRequest;
import org.entando.entando.plugins.jpattivita.web.attivita.validator.AttivitaValidator;

@RestController
@RequestMapping(value = "/jpattivita/attivitas")
public class AttivitaController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private IAttivitaService attivitaService;

    @Autowired
    private AttivitaValidator attivitaValidator;

    protected IAttivitaService getAttivitaService() {
        return attivitaService;
    }

    public void setAttivitaService(IAttivitaService attivitaService) {
        this.attivitaService = attivitaService;
    }

    protected AttivitaValidator getAttivitaValidator() {
        return attivitaValidator;
    }

    public void setAttivitaValidator(AttivitaValidator attivitaValidator) {
        this.attivitaValidator = attivitaValidator;
    }

    @RestAccessControl(permission = "superuser")
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PagedRestResponse<AttivitaDto>> getAttivitas(RestListRequest requestList) throws JsonProcessingException {
        this.getAttivitaValidator().validateRestListRequest(requestList, AttivitaDto.class);
        PagedMetadata<AttivitaDto> result = this.getAttivitaService().getAttivitas(requestList);
        this.getAttivitaValidator().validateRestListResult(requestList, result);
        logger.debug("Main Response -> {}", result);
        return new ResponseEntity<>(new PagedRestResponse<>(result), HttpStatus.OK);
    }

    @RestAccessControl(permission = "superuser")
    @RequestMapping(value = "/{attivitaId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SimpleRestResponse<AttivitaDto>> getAttivita(@PathVariable String attivitaId) {
		AttivitaDto attivita = this.getAttivitaService().getAttivita(Integer.valueOf(attivitaId));
        return new ResponseEntity<>(new SimpleRestResponse<>(attivita), HttpStatus.OK);
    }

    @RestAccessControl(permission = "superuser")
    @RequestMapping(value = "/{attivitaId}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SimpleRestResponse<AttivitaDto>> updateAttivita(@PathVariable String attivitaId, @Valid @RequestBody AttivitaRequest attivitaRequest, BindingResult bindingResult) {
        //field validations
        if (bindingResult.hasErrors()) {
            throw new ValidationGenericException(bindingResult);
        }
        this.getAttivitaValidator().validateBodyName(String.valueOf(attivitaId), attivitaRequest, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new ValidationGenericException(bindingResult);
        }

        AttivitaDto attivita = this.getAttivitaService().updateAttivita(attivitaRequest);
        return new ResponseEntity<>(new SimpleRestResponse<>(attivita), HttpStatus.OK);
    }

    @RestAccessControl(permission = "superuser")
    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SimpleRestResponse<AttivitaDto>> addAttivita(@Valid @RequestBody AttivitaRequest attivitaRequest, BindingResult bindingResult) {
        //field validations
        if (bindingResult.hasErrors()) {
            throw new ValidationGenericException(bindingResult);
        }
        //business validations
        getAttivitaValidator().validate(attivitaRequest, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new ValidationConflictException(bindingResult);
        }
        AttivitaDto dto = this.getAttivitaService().addAttivita(attivitaRequest);
        return new ResponseEntity<>(new SimpleRestResponse<>(dto), HttpStatus.OK);
    }

    @RestAccessControl(permission = "superuser")
    @RequestMapping(value = "/{attivitaId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SimpleRestResponse<Map>> deleteAttivita(@PathVariable String attivitaId) {
        logger.info("deleting {}", attivitaId);
        this.getAttivitaService().removeAttivita(Integer.valueOf(attivitaId));
        Map<String, Integer> result = new HashMap<>();
        result.put("id", Integer.valueOf(attivitaId));
        return new ResponseEntity<>(new SimpleRestResponse<>(result), HttpStatus.OK);
    }

}

