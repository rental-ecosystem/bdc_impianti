/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpsito.aps.system.services.sito;

import  org.entando.entando.plugins.jpsito.aps.system.services.sito.model.SitoDto;
import org.entando.entando.web.common.model.PagedMetadata;
import org.entando.entando.web.common.model.RestListRequest;
import org.entando.entando.plugins.jpsito.web.sito.model.SitoRequest;
import org.entando.entando.plugins.jpsito.web.sito.validator.SitoValidator;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import com.agiletec.aps.system.common.FieldSearchFilter;
import com.agiletec.aps.system.exception.ApsSystemException;
import com.agiletec.aps.system.common.model.dao.SearcherDaoPaginatedResult;
import org.entando.entando.aps.system.exception.RestServerError;
import org.entando.entando.web.common.exceptions.ValidationGenericException;
import org.entando.entando.aps.system.services.DtoBuilder;
import org.entando.entando.aps.system.services.IDtoBuilder;
import org.entando.entando.web.common.model.PagedMetadata;
import org.entando.entando.web.common.model.RestListRequest;
import org.entando.entando.aps.system.exception.ResourceNotFoundException;
import org.springframework.validation.BeanPropertyBindingResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

public class SitoService implements ISitoService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private ISitoManager sitoManager;
    private IDtoBuilder<Sito, SitoDto> dtoBuilder;


    protected ISitoManager getSitoManager() {
        return sitoManager;
    }

    public void setSitoManager(ISitoManager sitoManager) {
        this.sitoManager = sitoManager;
    }

    protected IDtoBuilder<Sito, SitoDto> getDtoBuilder() {
        return dtoBuilder;
    }

    public void setDtoBuilder(IDtoBuilder<Sito, SitoDto> dtoBuilder) {
        this.dtoBuilder = dtoBuilder;
    }

    @PostConstruct
    public void onInit() {
        this.setDtoBuilder(new DtoBuilder<Sito, SitoDto>() {

            @Override
            protected SitoDto toDto(Sito src) {
                SitoDto dto = new SitoDto();
                BeanUtils.copyProperties(src, dto);
                return dto;
            }
        });
    }

    @Override
    public PagedMetadata<SitoDto> getSitos(RestListRequest requestList) {
        try {
            List<FieldSearchFilter> filters = new ArrayList<FieldSearchFilter>(requestList.buildFieldSearchFilters());
            filters
                   .stream()
                   .filter(i -> i.getKey() != null)
                   .forEach(i -> i.setKey(SitoDto.getEntityFieldName(i.getKey())));

            SearcherDaoPaginatedResult<Sito> sitos = this.getSitoManager().getSitos(filters);
            List<SitoDto> dtoList = dtoBuilder.convert(sitos.getList());

            PagedMetadata<SitoDto> pagedMetadata = new PagedMetadata<>(requestList, sitos);
            pagedMetadata.setBody(dtoList);

            return pagedMetadata;
        } catch (Throwable t) {
            logger.error("error in search sitos", t);
            throw new RestServerError("error in search sitos", t);
        }
    }

    @Override
    public SitoDto updateSito(SitoRequest sitoRequest) {
        try {
	        Sito sito = this.getSitoManager().getSito(sitoRequest.getId());
	        if (null == sito) {
	            throw new ResourceNotFoundException(SitoValidator.ERRCODE_SITO_NOT_FOUND, "sito", String.valueOf(sitoRequest.getId()));
	        }
        	BeanUtils.copyProperties(sitoRequest, sito);
            BeanPropertyBindingResult validationResult = this.validateForUpdate(sito);
            if (validationResult.hasErrors()) {
                throw new ValidationGenericException(validationResult);
            }
            this.getSitoManager().updateSito(sito);
            return this.getDtoBuilder().convert(sito);
        } catch (ApsSystemException e) {
            logger.error("Error updating sito {}", sitoRequest.getId(), e);
            throw new RestServerError("error in update sito", e);
        }
    }

    @Override
    public SitoDto addSito(SitoRequest sitoRequest) {
        try {
            Sito sito = this.createSito(sitoRequest);
            BeanPropertyBindingResult validationResult = this.validateForAdd(sito);
            if (validationResult.hasErrors()) {
                throw new ValidationGenericException(validationResult);
            }
            this.getSitoManager().addSito(sito);
            SitoDto dto = this.getDtoBuilder().convert(sito);
            return dto;
        } catch (ApsSystemException e) {
            logger.error("Error adding a sito", e);
            throw new RestServerError("error in add sito", e);
        }
    }

    @Override
    public void removeSito(int  id) {
        try {
            Sito sito = this.getSitoManager().getSito(id);
            if (null == sito) {
                logger.info("sito {} does not exists", id);
                return;
            }
            BeanPropertyBindingResult validationResult = this.validateForDelete(sito);
            if (validationResult.hasErrors()) {
                throw new ValidationGenericException(validationResult);
            }
            this.getSitoManager().deleteSito(id);
        } catch (ApsSystemException e) {
            logger.error("Error in delete sito {}", id, e);
            throw new RestServerError("error in delete sito", e);
        }
    }

    @Override
    public SitoDto getSito(int  id) {
        try {
	        Sito sito = this.getSitoManager().getSito(id);
	        if (null == sito) {
	            logger.warn("no sito found with code {}", id);
	            throw new ResourceNotFoundException(SitoValidator.ERRCODE_SITO_NOT_FOUND, "sito", String.valueOf(id));
	        }
	        SitoDto dto = this.getDtoBuilder().convert(sito);
	        return dto;
        } catch (ApsSystemException e) {
            logger.error("Error loading sito {}", id, e);
            throw new RestServerError("error in loading sito", e);
        }
    }

    private Sito createSito(SitoRequest sitoRequest) {
        Sito sito = new Sito();
        BeanUtils.copyProperties(sitoRequest, sito);
        return sito;
    }


    protected BeanPropertyBindingResult validateForAdd(Sito sito) {
        BeanPropertyBindingResult errors = new BeanPropertyBindingResult(sito, "sito");
        return errors;
    }

    protected BeanPropertyBindingResult validateForDelete(Sito sito) {
        BeanPropertyBindingResult errors = new BeanPropertyBindingResult(sito, "sito");
        return errors;
    }

    protected BeanPropertyBindingResult validateForUpdate(Sito sito) {
        BeanPropertyBindingResult errors = new BeanPropertyBindingResult(sito, "sito");
        return errors;
    }

}

