/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpsito.aps.system.init.servdb;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = Sito.TABLE_NAME)
public class Sito {
	
	public Sito() {}
	
	@DatabaseField(columnName = "id_sito", 
		dataType = DataType.INTEGER, 
		 canBeNull=false, id = true)
	private int _id_sito;
	
	@DatabaseField(columnName = "codicesito", 
		dataType = DataType.STRING, 
		 canBeNull=false)
	private String _codiceSito;
	
	@DatabaseField(columnName = "descrizionesito", 
		dataType = DataType.LONG_STRING,
		 canBeNull=false)
	private String _descrizioneSito;
	
	@DatabaseField(columnName = "regionesito", 
		dataType = DataType.LONG_STRING,
		 canBeNull=false)
	private String _regioneSito;
	

public static final String TABLE_NAME = "sito";
}
