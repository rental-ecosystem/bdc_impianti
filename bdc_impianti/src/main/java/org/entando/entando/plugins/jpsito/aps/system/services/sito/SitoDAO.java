/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpsito.aps.system.services.sito;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import com.agiletec.aps.system.common.AbstractSearcherDAO;
import com.agiletec.aps.system.common.FieldSearchFilter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SitoDAO extends AbstractSearcherDAO implements ISitoDAO {

	private static final Logger logger =  LoggerFactory.getLogger(SitoDAO.class);

    @Override
    public int countSitos(FieldSearchFilter[] filters) {
        Integer sitos = null;
        try {
            sitos = super.countId(filters);
        } catch (Throwable t) {
            logger.error("error in count sitos", t);
            throw new RuntimeException("error in count sitos", t);
        }
        return sitos;
    }

	@Override
	protected String getTableFieldName(String metadataFieldKey) {
		return metadataFieldKey;
	}
	
	@Override
	protected String getMasterTableName() {
		return "sito";
	}
	
	@Override
	protected String getMasterTableIdFieldName() {
		return "id_sito";
	}

    @Override
    public List<Integer> searchSitos(FieldSearchFilter[] filters) {
            List<Integer> sitosId = new ArrayList<>();
        List<String> masterList = super.searchId(filters);
        masterList.stream().forEach(idString -> sitosId.add(Integer.parseInt(idString)));
        return sitosId;
        }


	@Override
	public List<Integer> loadSitos() {
		List<Integer> sitosId = new ArrayList<Integer>();
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet res = null;
		try {
			conn = this.getConnection();
			stat = conn.prepareStatement(LOAD_SITOS_ID);
			res = stat.executeQuery();
			while (res.next()) {
				int id = res.getInt("id");
				sitosId.add(id);
			}
		} catch (Throwable t) {
			logger.error("Error loading Sito list",  t);
			throw new RuntimeException("Error loading Sito list", t);
		} finally {
			closeDaoResources(res, stat, conn);
		}
		return sitosId;
	}
	
	@Override
	public void insertSito(Sito sito) {
		PreparedStatement stat = null;
		Connection conn  = null;
		try {
			conn = this.getConnection();
			conn.setAutoCommit(false);
			this.insertSito(sito, conn);
 			conn.commit();
		} catch (Throwable t) {
			this.executeRollback(conn);
			logger.error("Error on insert sito",  t);
			throw new RuntimeException("Error on insert sito", t);
		} finally {
			this.closeDaoResources(null, stat, conn);
		}
	}

	public void insertSito(Sito sito, Connection conn) {
		PreparedStatement stat = null;
		try {
			stat = conn.prepareStatement(ADD_SITO);
			int index = 1;
			stat.setInt(index++, sito.getId());
 			stat.setString(index++, sito.getCodiceSito());
 			stat.setString(index++, sito.getDescrizioneSito());
 			stat.setString(index++, sito.getRegioneSito());
			stat.executeUpdate();
		} catch (Throwable t) {
			logger.error("Error on insert sito",  t);
			throw new RuntimeException("Error on insert sito", t);
		} finally {
			this.closeDaoResources(null, stat, null);
		}
	}

	@Override
	public void updateSito(Sito sito) {
		PreparedStatement stat = null;
		Connection conn = null;
		try {
			conn = this.getConnection();
			conn.setAutoCommit(false);
			this.updateSito(sito, conn);
 			conn.commit();
		} catch (Throwable t) {
			this.executeRollback(conn);
			logger.error("Error updating sito {}", sito.getId(),  t);
			throw new RuntimeException("Error updating sito", t);
		} finally {
			this.closeDaoResources(null, stat, conn);
		}
	}

	public void updateSito(Sito sito, Connection conn) {
		PreparedStatement stat = null;
		try {
			stat = conn.prepareStatement(UPDATE_SITO);
			int index = 1;

 			stat.setString(index++, sito.getCodiceSito());
 			stat.setString(index++, sito.getDescrizioneSito());
 			stat.setString(index++, sito.getRegioneSito());
			stat.setInt(index++, sito.getId());
			stat.executeUpdate();
		} catch (Throwable t) {
			logger.error("Error updating sito {}", sito.getId(),  t);
			throw new RuntimeException("Error updating sito", t);
		} finally {
			this.closeDaoResources(null, stat, null);
		}
	}

	@Override
	public void removeSito(int id) {
		PreparedStatement stat = null;
		Connection conn = null;
		try {
			conn = this.getConnection();
			conn.setAutoCommit(false);
			this.removeSito(id, conn);
 			conn.commit();
		} catch (Throwable t) {
			this.executeRollback(conn);
			logger.error("Error deleting sito {}", id, t);
			throw new RuntimeException("Error deleting sito", t);
		} finally {
			this.closeDaoResources(null, stat, conn);
		}
	}
	
	public void removeSito(int id, Connection conn) {
		PreparedStatement stat = null;
		try {
			stat = conn.prepareStatement(DELETE_SITO);
			int index = 1;
			stat.setInt(index++, id);
			stat.executeUpdate();
		} catch (Throwable t) {
			logger.error("Error deleting sito {}", id, t);
			throw new RuntimeException("Error deleting sito", t);
		} finally {
			this.closeDaoResources(null, stat, null);
		}
	}

	public Sito loadSito(int id) {
		Sito sito = null;
		Connection conn = null;
		PreparedStatement stat = null;
		ResultSet res = null;
		try {
			conn = this.getConnection();
			sito = this.loadSito(id, conn);
		} catch (Throwable t) {
			logger.error("Error loading sito with id {}", id, t);
			throw new RuntimeException("Error loading sito with id " + id, t);
		} finally {
			closeDaoResources(res, stat, conn);
		}
		return sito;
	}

	public Sito loadSito(int id, Connection conn) {
		Sito sito = null;
		PreparedStatement stat = null;
		ResultSet res = null;
		try {
			stat = conn.prepareStatement(LOAD_SITO);
			int index = 1;
			stat.setInt(index++, id);
			res = stat.executeQuery();
			if (res.next()) {
				sito = this.buildSitoFromRes(res);
			}
		} catch (Throwable t) {
			logger.error("Error loading sito with id {}", id, t);
			throw new RuntimeException("Error loading sito with id " + id, t);
		} finally {
			closeDaoResources(res, stat, null);
		}
		return sito;
	}

	protected Sito buildSitoFromRes(ResultSet res) {
		Sito sito = null;
		try {
			sito = new Sito();				
			sito.setId(res.getInt("id_sito"));
			sito.setCodiceSito(res.getString("codicesito"));
			sito.setDescrizioneSito(res.getString("descrizionesito"));
			sito.setRegioneSito(res.getString("regionesito"));
		} catch (Throwable t) {
			logger.error("Error in buildSitoFromRes", t);
		}
		return sito;
	}

	private static final String ADD_SITO = "INSERT INTO sito (id_sito, codicesito, descrizionesito, regionesito ) VALUES (?, ?, ?, ? )";

	private static final String UPDATE_SITO = "UPDATE sito SET  codicesito=?,  descrizionesito=?, regionesito=? WHERE id_sito = ?";

	private static final String DELETE_SITO = "DELETE FROM sito WHERE id_sito = ?";
	
	private static final String LOAD_SITO = "SELECT id, codicesito, descrizionesito, regionesito  FROM sito WHERE id_sito = ?";
	
	private static final String LOAD_SITOS_ID  = "SELECT id_sito FROM sito";
	
}