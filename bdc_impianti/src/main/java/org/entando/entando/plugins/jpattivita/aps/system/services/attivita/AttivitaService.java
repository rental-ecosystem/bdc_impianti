/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpattivita.aps.system.services.attivita;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;

import org.entando.entando.aps.system.exception.ResourceNotFoundException;
import org.entando.entando.aps.system.exception.RestServerError;
import org.entando.entando.aps.system.services.DtoBuilder;
import org.entando.entando.aps.system.services.IDtoBuilder;
import  org.entando.entando.plugins.jpattivita.aps.system.services.attivita.model.AttivitaDto;
import org.entando.entando.plugins.jpattivita.web.attivita.model.AttivitaRequest;
import org.entando.entando.plugins.jpattivita.web.attivita.validator.AttivitaValidator;
import org.entando.entando.web.common.exceptions.ValidationGenericException;
import org.entando.entando.web.common.model.PagedMetadata;
import org.entando.entando.web.common.model.RestListRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BeanPropertyBindingResult;

import com.agiletec.aps.system.common.FieldSearchFilter;
import com.agiletec.aps.system.common.model.dao.SearcherDaoPaginatedResult;
import com.agiletec.aps.system.exception.ApsSystemException;

public class AttivitaService implements IAttivitaService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private IAttivitaManager attivitaManager;
    private IDtoBuilder<Attivita, AttivitaDto> dtoBuilder;


    protected IAttivitaManager getAttivitaManager() {
        return attivitaManager;
    }

    public void setAttivitaManager(IAttivitaManager attivitaManager) {
        this.attivitaManager = attivitaManager;
    }

    protected IDtoBuilder<Attivita, AttivitaDto> getDtoBuilder() {
        return dtoBuilder;
    }

    public void setDtoBuilder(IDtoBuilder<Attivita, AttivitaDto> dtoBuilder) {
        this.dtoBuilder = dtoBuilder;
    }

    @PostConstruct
    public void onInit() {
        this.setDtoBuilder(new DtoBuilder<Attivita, AttivitaDto>() {

            @Override
            protected AttivitaDto toDto(Attivita src) {
                AttivitaDto dto = new AttivitaDto();
                BeanUtils.copyProperties(src, dto);
                return dto;
            }
        });
    }

    @Override
    public PagedMetadata<AttivitaDto> getAttivitas(RestListRequest requestList) {
        try {
            List<FieldSearchFilter> filters = new ArrayList<FieldSearchFilter>(requestList.buildFieldSearchFilters());
            filters
                   .stream()
                   .filter(i -> i.getKey() != null)
                   .forEach(i -> i.setKey(AttivitaDto.getEntityFieldName(i.getKey())));

            SearcherDaoPaginatedResult<Attivita> attivitas = this.getAttivitaManager().getAttivitas(filters);
            List<AttivitaDto> dtoList = dtoBuilder.convert(attivitas.getList());

            PagedMetadata<AttivitaDto> pagedMetadata = new PagedMetadata<>(requestList, attivitas);
            pagedMetadata.setBody(dtoList);

            return pagedMetadata;
        } catch (Throwable t) {
            logger.error("error in search attivitas", t);
            throw new RestServerError("error in search attivitas", t);
        }
    }

    @Override
    public AttivitaDto updateAttivita(AttivitaRequest attivitaRequest) {
        try {
	        Attivita attivita = this.getAttivitaManager().getAttivita(attivitaRequest.getId_att());
	        if (null == attivita) {
	            throw new ResourceNotFoundException(AttivitaValidator.ERRCODE_ATTIVITA_NOT_FOUND, "attivita", String.valueOf(attivitaRequest.getId_att()));
	        }
        	BeanUtils.copyProperties(attivitaRequest, attivita);
            BeanPropertyBindingResult validationResult = this.validateForUpdate(attivita);
            if (validationResult.hasErrors()) {
                throw new ValidationGenericException(validationResult);
            }
            this.getAttivitaManager().updateAttivita(attivita);
            return this.getDtoBuilder().convert(attivita);
        } catch (ApsSystemException e) {
            logger.error("Error updating attivita {}", attivitaRequest.getId_att(), e);
            throw new RestServerError("error in update attivita", e);
        }
    }

    @Override
    public AttivitaDto addAttivita(AttivitaRequest attivitaRequest) {
        try {
            Attivita attivita = this.createAttivita(attivitaRequest);
            BeanPropertyBindingResult validationResult = this.validateForAdd(attivita);
            if (validationResult.hasErrors()) {
                throw new ValidationGenericException(validationResult);
            }
            this.getAttivitaManager().addAttivita(attivita);
            AttivitaDto dto = this.getDtoBuilder().convert(attivita);
            return dto;
        } catch (ApsSystemException e) {
            logger.error("Error adding a attivita", e);
            throw new RestServerError("error in add attivita", e);
        }
    }
    
    @Override
    public void addAttivitaList(List<AttivitaRequest> attivitaRequestList) {
        try {
        	List<Attivita> attivitaList = new ArrayList<Attivita>();
        	for(AttivitaRequest attivitaRequest : attivitaRequestList) {
                Attivita attivita = this.createAttivita(attivitaRequest);
                BeanPropertyBindingResult validationResult = this.validateForAdd(attivita);
                if (validationResult.hasErrors()) {
                    throw new ValidationGenericException(validationResult);
                }
                attivitaList.add(attivita);
        	}
            this.getAttivitaManager().addAttivitaList(attivitaList);
        } catch (ApsSystemException e) {
            logger.error("Error adding a attivita", e);
            throw new RestServerError("error in add attivita", e);
        }
    }

    @Override
    public void removeAttivita(int  id) {
        try {
            Attivita attivita = this.getAttivitaManager().getAttivita(id);
            if (null == attivita) {
                logger.info("attivita {} does not exists", id);
                return;
            }
            BeanPropertyBindingResult validationResult = this.validateForDelete(attivita);
            if (validationResult.hasErrors()) {
                throw new ValidationGenericException(validationResult);
            }
            this.getAttivitaManager().deleteAttivita(id);
        } catch (ApsSystemException e) {
            logger.error("Error in delete attivita {}", id, e);
            throw new RestServerError("error in delete attivita", e);
        }
    }

    @Override
    public AttivitaDto getAttivita(int  id) {
        try {
	        Attivita attivita = this.getAttivitaManager().getAttivita(id);
	        if (null == attivita) {
	            logger.warn("no attivita found with code {}", id);
	            throw new ResourceNotFoundException(AttivitaValidator.ERRCODE_ATTIVITA_NOT_FOUND, "attivita", String.valueOf(id));
	        }
	        AttivitaDto dto = this.getDtoBuilder().convert(attivita);
	        return dto;
        } catch (ApsSystemException e) {
            logger.error("Error loading attivita {}", id, e);
            throw new RestServerError("error in loading attivita", e);
        }
    }

    private Attivita createAttivita(AttivitaRequest attivitaRequest) {
        Attivita attivita = new Attivita();
        BeanUtils.copyProperties(attivitaRequest, attivita);
        return attivita;
    }


    protected BeanPropertyBindingResult validateForAdd(Attivita attivita) {
        BeanPropertyBindingResult errors = new BeanPropertyBindingResult(attivita, "attivita");
        return errors;
    }

    protected BeanPropertyBindingResult validateForDelete(Attivita attivita) {
        BeanPropertyBindingResult errors = new BeanPropertyBindingResult(attivita, "attivita");
        return errors;
    }

    protected BeanPropertyBindingResult validateForUpdate(Attivita attivita) {
        BeanPropertyBindingResult errors = new BeanPropertyBindingResult(attivita, "attivita");
        return errors;
    }

}

