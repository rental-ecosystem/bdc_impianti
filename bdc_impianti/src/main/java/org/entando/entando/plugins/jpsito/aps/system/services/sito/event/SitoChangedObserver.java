/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpsito.aps.system.services.sito.event;

import com.agiletec.aps.system.common.notify.ObserverService;

public interface SitoChangedObserver extends ObserverService {
	
	public void updateFromSitoChanged(SitoChangedEvent event);
	
}
