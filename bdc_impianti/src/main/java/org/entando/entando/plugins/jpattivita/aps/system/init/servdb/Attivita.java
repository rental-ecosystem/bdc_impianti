/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpattivita.aps.system.init.servdb;
import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = Attivita.TABLE_NAME)
public class Attivita {
	
	public Attivita() {}
	
	
	@DatabaseField(columnName = "id_att", 
		dataType = DataType.INTEGER, 
		 canBeNull=false, id = true)
	private int _id_att;
	
	@DatabaseField(columnName = "codiceriferimentoatt", 
		dataType = DataType.LONG_STRING,
		 canBeNull=false)
	private String _codiceRiferimentoAtt;
	
	@DatabaseField(columnName = "descrizioneatt", 
		dataType = DataType.LONG_STRING,
		 canBeNull=false)
	private String _descrizioneAtt;
	
	@DatabaseField(columnName = "descrizionemacroatt", 
		dataType = DataType.LONG_STRING,
		 canBeNull=false)
	private String _descrizioneMacroAtt;
	
	@DatabaseField(columnName = "impattoattivita", 
		dataType = DataType.INTEGER, 
		 canBeNull=false)
	private int _impattoAttivita;
	
	@DatabaseField(columnName = "noteattivita", 
		dataType = DataType.LONG_STRING,
		 canBeNull=false)
	private String _noteAttivita;
	
	@DatabaseField(columnName = "fkvalutazioneatt", 
		dataType = DataType.INTEGER, 
		 canBeNull=false)
	private int _fkValutazioneAtt;
	
	@DatabaseField(columnName = "fkauditatt", 
		dataType = DataType.INTEGER, 
		 canBeNull=false)
	private int _fkAuditAtt;
	

public static final String TABLE_NAME = "attivita";
}
