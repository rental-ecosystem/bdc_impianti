/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpsito.aps.system.services.sito;



public class Sito {

	public int getId() {
		return _id;
	}
	public void setId(int id) {
		this._id = id;
	}

	public String getCodiceSito() {
		return _codiceSito;
	}
	public void setCodiceSito(String codiceSito) {
		this._codiceSito = codiceSito;
	}

	public String getDescrizioneSito() {
		return _descrizioneSito;
	}
	public void setDescrizioneSito(String descrizioneSito) {
		this._descrizioneSito = descrizioneSito;
	}

	public String getRegioneSito() {
		return _regioneSito;
	}
	public void setRegioneSito(String regioneSito) {
		this._regioneSito = regioneSito;
	}

	
	private int _id;
	private String _codiceSito;
	private String _descrizioneSito;
	private String _regioneSito;

}
