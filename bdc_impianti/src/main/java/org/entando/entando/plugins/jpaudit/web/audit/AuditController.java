/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpaudit.web.audit;

import java.util.HashMap;
import java.util.Map;

import javax.validation.Valid;

import com.agiletec.aps.system.services.role.Permission;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.entando.entando.web.common.annotation.RestAccessControl;
import org.entando.entando.web.common.exceptions.ValidationConflictException;
import org.entando.entando.web.common.exceptions.ValidationGenericException;
import org.entando.entando.web.common.model.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import org.entando.entando.plugins.jpaudit.aps.system.services.audit.IAuditService;
import org.entando.entando.plugins.jpaudit.aps.system.services.audit.model.AuditDto;
import org.entando.entando.plugins.jpaudit.web.audit.model.AuditRequest;
import org.entando.entando.plugins.jpaudit.web.audit.validator.AuditValidator;

@RestController
@RequestMapping(value = "/jpaudit/audits")
public class AuditController {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private IAuditService auditService;

    @Autowired
    private AuditValidator auditValidator;

    protected IAuditService getAuditService() {
        return auditService;
    }

    public void setAuditService(IAuditService auditService) {
        this.auditService = auditService;
    }

    protected AuditValidator getAuditValidator() {
        return auditValidator;
    }

    public void setAuditValidator(AuditValidator auditValidator) {
        this.auditValidator = auditValidator;
    }

    @RestAccessControl(permission = "superuser")
    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<PagedRestResponse<AuditDto>> getAudits(RestListRequest requestList) throws JsonProcessingException {
        this.getAuditValidator().validateRestListRequest(requestList, AuditDto.class);
        PagedMetadata<AuditDto> result = this.getAuditService().getAudits(requestList);
        this.getAuditValidator().validateRestListResult(requestList, result);
        logger.debug("Main Response -> {}", result);
        return new ResponseEntity<>(new PagedRestResponse<>(result), HttpStatus.OK);
    }

    @RestAccessControl(permission = "superuser")
    @RequestMapping(value = "/{auditId}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SimpleRestResponse<AuditDto>> getAudit(@PathVariable String auditId) {
		AuditDto audit = this.getAuditService().getAudit(Integer.valueOf(auditId));
        return new ResponseEntity<>(new SimpleRestResponse<>(audit), HttpStatus.OK);
    }

    @RestAccessControl(permission = "superuser")
    @RequestMapping(value = "/{auditId}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SimpleRestResponse<AuditDto>> updateAudit(@PathVariable String auditId, @Valid @RequestBody AuditRequest auditRequest, BindingResult bindingResult) {
        //field validations
        if (bindingResult.hasErrors()) {
            throw new ValidationGenericException(bindingResult);
        }
        this.getAuditValidator().validateBodyName(String.valueOf(auditId), auditRequest, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new ValidationGenericException(bindingResult);
        }

        AuditDto audit = this.getAuditService().updateAudit(auditRequest);
        return new ResponseEntity<>(new SimpleRestResponse<>(audit), HttpStatus.OK);
    }

    @RestAccessControl(permission = "superuser")
    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SimpleRestResponse<AuditDto>> addAudit(@Valid @RequestBody AuditRequest auditRequest, BindingResult bindingResult) {
        //field validations
        if (bindingResult.hasErrors()) {
            throw new ValidationGenericException(bindingResult);
        }
        //business validations
        getAuditValidator().validate(auditRequest, bindingResult);
        if (bindingResult.hasErrors()) {
            throw new ValidationConflictException(bindingResult);
        }
        AuditDto dto = this.getAuditService().addAudit(auditRequest);
        return new ResponseEntity<>(new SimpleRestResponse<>(dto), HttpStatus.OK);
    }

    @RestAccessControl(permission = "superuser")
    @RequestMapping(value = "/{auditId}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<SimpleRestResponse<Map>> deleteAudit(@PathVariable String auditId) {
        logger.info("deleting {}", auditId);
        this.getAuditService().removeAudit(Integer.valueOf(auditId));
        Map<String, Integer> result = new HashMap<>();
        result.put("id", Integer.valueOf(auditId));
        return new ResponseEntity<>(new SimpleRestResponse<>(result), HttpStatus.OK);
    }

}

