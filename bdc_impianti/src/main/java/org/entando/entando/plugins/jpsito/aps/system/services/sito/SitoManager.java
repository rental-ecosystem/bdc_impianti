/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpsito.aps.system.services.sito;

import org.entando.entando.plugins.jpsito.aps.system.services.sito.event.SitoChangedEvent;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.agiletec.aps.system.common.FieldSearchFilter;
import com.agiletec.aps.system.common.AbstractService;
import com.agiletec.aps.system.exception.ApsSystemException;
import com.agiletec.aps.system.services.keygenerator.IKeyGeneratorManager;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.agiletec.aps.system.common.model.dao.SearcherDaoPaginatedResult;

public class SitoManager extends AbstractService implements ISitoManager {

	private static final Logger logger =  LoggerFactory.getLogger(SitoManager.class);

	@Override
	public void init() throws Exception {
		logger.debug("{} ready.", this.getClass().getName());
	}
 
	@Override
	public Sito getSito(int id) throws ApsSystemException {
		Sito sito = null;
		try {
			sito = this.getSitoDAO().loadSito(id);
		} catch (Throwable t) {
			logger.error("Error loading sito with id '{}'", id,  t);
			throw new ApsSystemException("Error loading sito with id: " + id, t);
		}
		return sito;
	}

	@Override
	public List<Integer> getSitos() throws ApsSystemException {
		List<Integer> sitos = new ArrayList<Integer>();
		try {
			sitos = this.getSitoDAO().loadSitos();
		} catch (Throwable t) {
			logger.error("Error loading Sito list",  t);
			throw new ApsSystemException("Error loading Sito ", t);
		}
		return sitos;
	}

	@Override
	public List<Integer> searchSitos(FieldSearchFilter filters[]) throws ApsSystemException {
		List<Integer> sitos = new ArrayList<Integer>();
		try {
			sitos = this.getSitoDAO().searchSitos(filters);
		} catch (Throwable t) {
			logger.error("Error searching Sitos", t);
			throw new ApsSystemException("Error searching Sitos", t);
		}
		return sitos;
	}

	@Override
	public void addSito(Sito sito) throws ApsSystemException {
		try {
			int key = this.getKeyGeneratorManager().getUniqueKeyCurrentValue();
			sito.setId(key);
			this.getSitoDAO().insertSito(sito);
			this.notifySitoChangedEvent(sito, SitoChangedEvent.INSERT_OPERATION_CODE);
		} catch (Throwable t) {
			logger.error("Error adding Sito", t);
			throw new ApsSystemException("Error adding Sito", t);
		}
	}
 
	@Override
	public void updateSito(Sito sito) throws ApsSystemException {
		try {
			this.getSitoDAO().updateSito(sito);
			this.notifySitoChangedEvent(sito, SitoChangedEvent.UPDATE_OPERATION_CODE);
		} catch (Throwable t) {
			logger.error("Error updating Sito", t);
			throw new ApsSystemException("Error updating Sito " + sito, t);
		}
	}

	@Override
	public void deleteSito(int id) throws ApsSystemException {
		try {
			Sito sito = this.getSito(id);
			this.getSitoDAO().removeSito(id);
			this.notifySitoChangedEvent(sito, SitoChangedEvent.REMOVE_OPERATION_CODE);
		} catch (Throwable t) {
			logger.error("Error deleting Sito with id {}", id, t);
			throw new ApsSystemException("Error deleting Sito with id:" + id, t);
		}
	}


	private void notifySitoChangedEvent(Sito sito, int operationCode) {
		SitoChangedEvent event = new SitoChangedEvent();
		event.setSito(sito);
		event.setOperationCode(operationCode);
		this.notifyEvent(event);
	}

    @SuppressWarnings("rawtypes")
    public SearcherDaoPaginatedResult<Sito> getSitos(FieldSearchFilter[] filters) throws ApsSystemException {
        SearcherDaoPaginatedResult<Sito> pagedResult = null;
        try {
            List<Sito> sitos = new ArrayList<>();
            int count = this.getSitoDAO().countSitos(filters);

            List<Integer> sitoNames = this.getSitoDAO().searchSitos(filters);
            for (Integer sitoName : sitoNames) {
                sitos.add(this.getSito(sitoName));
            }
            pagedResult = new SearcherDaoPaginatedResult<Sito>(count, sitos);
        } catch (Throwable t) {
            logger.error("Error searching sitos", t);
            throw new ApsSystemException("Error searching sitos", t);
        }
        return pagedResult;
    }

    @Override
    public SearcherDaoPaginatedResult<Sito> getSitos(List<FieldSearchFilter> filters) throws ApsSystemException {
        FieldSearchFilter[] array = null;
        if (null != filters) {
            array = filters.toArray(new FieldSearchFilter[filters.size()]);
        }
        return this.getSitos(array);
    }


	protected IKeyGeneratorManager getKeyGeneratorManager() {
		return _keyGeneratorManager;
	}
	public void setKeyGeneratorManager(IKeyGeneratorManager keyGeneratorManager) {
		this._keyGeneratorManager = keyGeneratorManager;
	}

	public void setSitoDAO(ISitoDAO sitoDAO) {
		 this._sitoDAO = sitoDAO;
	}
	protected ISitoDAO getSitoDAO() {
		return _sitoDAO;
	}

	private IKeyGeneratorManager _keyGeneratorManager;
	private ISitoDAO _sitoDAO;
}
