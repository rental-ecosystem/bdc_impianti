/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpaudit.aps.system.init.servdb;
import java.util.Date;

import com.j256.ormlite.field.DataType;
import com.j256.ormlite.field.DatabaseField;
import com.j256.ormlite.table.DatabaseTable;

@DatabaseTable(tableName = Audit.TABLE_NAME)
public class Audit {
	
	public Audit() {}
	
	@DatabaseField(columnName = "id_audit", 
		dataType = DataType.INTEGER, 
		 canBeNull=false, id = true)
	private int _id_audit;
	
	@DatabaseField(columnName = "valoreverificaaudit", 
		dataType = DataType.INTEGER, 
		 canBeNull=false)
	private int _valoreVerificaAudit;
	
	@DatabaseField(columnName = "ispettoreaudit", 
		dataType = DataType.LONG_STRING,
		 canBeNull=false)
	private String _ispettoreAudit;
	
	@DatabaseField(columnName = "notegeneraliaudit", 
		dataType = DataType.LONG_STRING,
		 canBeNull=false)
	private String _noteGeneraliAudit;
	
	@DatabaseField(columnName = "dataaudit", 
			dataType = DataType.DATE,
		 canBeNull=false)
	private Date _dataAudit;
	
	@DatabaseField(columnName = "datainserimentoaudit", 
			dataType = DataType.DATE,
		 canBeNull=false)
	private Date _dataInserimentoAudit;
	
	@DatabaseField(columnName = "datamodificaaudit", 
			dataType = DataType.DATE,
		 canBeNull=false)
	private Date _dataModificaAudit;
	
	@DatabaseField(columnName = "fkcodicesito", 
		dataType = DataType.LONG_STRING,
		 canBeNull=false)
	private String _fkCodiceSito;
	

public static final String TABLE_NAME = "auditsito";
}
