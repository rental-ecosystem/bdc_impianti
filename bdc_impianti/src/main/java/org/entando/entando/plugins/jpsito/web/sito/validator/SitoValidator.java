/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpsito.web.sito.validator;

import org.apache.commons.lang3.StringUtils;
import org.entando.entando.web.common.validator.AbstractPaginationValidator;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.entando.entando.plugins.jpsito.web.sito.model.SitoRequest;


@Component
public class SitoValidator extends AbstractPaginationValidator {

    public static final String ERRCODE_URINAME_MISMATCH = "1";
    public static final String ERRCODE_SITO_NOT_FOUND = "2" ;
    public static final String ERRCODE_SITO_ALREADY_EXISTS = "3";


    @Override
    public boolean supports(Class<?> paramClass) {
        return SitoRequest.class.equals(paramClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        //SitoRequest request = (SitoRequest) target;
    }

    public void validateBodyName(String sitoId, SitoRequest sitoRequest, Errors errors) {
        if (!StringUtils.equals(sitoId, String.valueOf(sitoRequest.getId()))) {
            errors.rejectValue("id", ERRCODE_URINAME_MISMATCH, new Object[]{sitoId, sitoRequest.getId()}, "sito.id.mismatch");
        }
    }

    @Override
    protected String getDefaultSortProperty() {
        return "id";
    }

}
