/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpsito.aps.system.services.sito;

import java.util.List;
import com.agiletec.aps.system.exception.ApsSystemException;
import com.agiletec.aps.system.common.model.dao.SearcherDaoPaginatedResult;

import com.agiletec.aps.system.common.FieldSearchFilter;

public interface ISitoManager {

	public Sito getSito(int id) throws ApsSystemException;

	public List<Integer> getSitos() throws ApsSystemException;

	public List<Integer> searchSitos(FieldSearchFilter filters[]) throws ApsSystemException;

	public void addSito(Sito sito) throws ApsSystemException;

	public void updateSito(Sito sito) throws ApsSystemException;

	public void deleteSito(int id) throws ApsSystemException;

	public SearcherDaoPaginatedResult<Sito> getSitos(List<FieldSearchFilter> fieldSearchFilters) throws ApsSystemException;
}