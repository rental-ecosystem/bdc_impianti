/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpaudit.aps.system.services.audit.event;

import com.agiletec.aps.system.common.IManager;
import com.agiletec.aps.system.common.notify.ApsEvent;
import org.entando.entando.plugins.jpaudit.aps.system.services.audit.Audit;


public class AuditChangedEvent extends ApsEvent {
	
	@Override
	public void notify(IManager srv) {
		((AuditChangedObserver) srv).updateFromAuditChanged(this);
	}
	
	@Override
	public Class getObserverInterface() {
		return AuditChangedObserver.class;
	}
	
	public int getOperationCode() {
		return _operationCode;
	}
	public void setOperationCode(int operationCode) {
		this._operationCode = operationCode;
	}
	
	public Audit getAudit() {
		return _audit;
	}
	public void setAudit(Audit audit) {
		this._audit = audit;
	}

	private Audit _audit;
	private int _operationCode;
	
	public static final int INSERT_OPERATION_CODE = 1;
	public static final int REMOVE_OPERATION_CODE = 2;
	public static final int UPDATE_OPERATION_CODE = 3;

}
