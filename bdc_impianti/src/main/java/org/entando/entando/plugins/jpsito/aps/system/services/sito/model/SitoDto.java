/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpsito.aps.system.services.sito.model;


import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.entando.entando.web.common.json.JsonDateDeserializer;
import org.entando.entando.web.common.json.JsonDateSerializer;

public class SitoDto {

	private int id;
	private String codiceSito;
	private String descrizioneSito;
	private String regioneSito;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getCodiceSito() {
		return codiceSito;
	}
	public void setCodiceSito(String codiceSito) {
		this.codiceSito = codiceSito;
	}

	public String getDescrizioneSito() {
		return descrizioneSito;
	}
	public void setDescrizioneSito(String descrizioneSito) {
		this.descrizioneSito = descrizioneSito;
	}

	public String getRegioneSito() {
		return regioneSito;
	}
	public void setRegioneSito(String regioneSito) {
		this.regioneSito = regioneSito;
	}

    public static String getEntityFieldName(String dtoFieldName) {
		return dtoFieldName;
    }
    
}
