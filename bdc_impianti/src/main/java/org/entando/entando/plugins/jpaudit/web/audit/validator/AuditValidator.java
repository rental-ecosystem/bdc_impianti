/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpaudit.web.audit.validator;

import org.apache.commons.lang3.StringUtils;
import org.entando.entando.web.common.validator.AbstractPaginationValidator;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.entando.entando.plugins.jpaudit.web.audit.model.AuditRequest;


@Component
public class AuditValidator extends AbstractPaginationValidator {

    public static final String ERRCODE_URINAME_MISMATCH = "1";
    public static final String ERRCODE_AUDIT_NOT_FOUND = "2" ;
    public static final String ERRCODE_AUDIT_ALREADY_EXISTS = "3";


    @Override
    public boolean supports(Class<?> paramClass) {
        return AuditRequest.class.equals(paramClass);
    }

    @Override
    public void validate(Object target, Errors errors) {
        //AuditRequest request = (AuditRequest) target;
    }

    public void validateBodyName(String auditId, AuditRequest auditRequest, Errors errors) {
        if (!StringUtils.equals(auditId, String.valueOf(auditRequest.getIdAudit()))) {
            errors.rejectValue("id", ERRCODE_URINAME_MISMATCH, new Object[]{auditId, auditRequest.getIdAudit()}, "audit.id.mismatch");
        }
    }

    @Override
    protected String getDefaultSortProperty() {
        return "id";
    }

}
