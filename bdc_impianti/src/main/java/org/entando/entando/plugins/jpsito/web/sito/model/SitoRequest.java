/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpsito.web.sito.model;


import javax.validation.constraints.*;
import org.hibernate.validator.constraints.NotBlank;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.entando.entando.web.common.json.JsonDateDeserializer;
import org.entando.entando.web.common.json.JsonDateSerializer;

public class SitoRequest {

    @NotNull(message = "sito.id.notBlank")	
	private int id;

	@NotBlank(message = "sito.codiceSito.notBlank")
	private String codiceSito;

	@NotBlank(message = "sito.descrizioneSito.notBlank")
	private String descrizioneSito;

	@NotBlank(message = "sito.regioneSito.notBlank")
	private String regioneSito;


	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getCodiceSito() {
		return codiceSito;
	}
	public void setCodiceSito(String codiceSito) {
		this.codiceSito = codiceSito;
	}

	public String getDescrizioneSito() {
		return descrizioneSito;
	}
	public void setDescrizioneSito(String descrizioneSito) {
		this.descrizioneSito = descrizioneSito;
	}

	public String getRegioneSito() {
		return regioneSito;
	}
	public void setRegioneSito(String regioneSito) {
		this.regioneSito = regioneSito;
	}


}
