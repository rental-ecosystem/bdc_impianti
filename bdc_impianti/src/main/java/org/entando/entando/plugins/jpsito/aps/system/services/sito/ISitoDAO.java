/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpsito.aps.system.services.sito;

import java.util.List;

import com.agiletec.aps.system.common.FieldSearchFilter;

public interface ISitoDAO {

	public List<Integer> searchSitos(FieldSearchFilter[] filters);
	
	public Sito loadSito(int id);

	public List<Integer> loadSitos();

	public void removeSito(int id);
	
	public void updateSito(Sito sito);

	public void insertSito(Sito sito);

    public int countSitos(FieldSearchFilter[] filters);
}