/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpattivita.aps.system.services.attivita.model;

public class AttivitaDto {

	private int id_att;
	private String codiceRiferimentoAtt;
	private String descrizioneAtt;
	private String descrizioneMacroAtt;
	private int impattoAttivita;
	private String noteAttivita;
	private int fkValutazioneAtt;
	private int fkAuditAtt;

	public int getId_att() {
		return id_att;
	}
	public void setId_att(int id_att) {
		this.id_att = id_att;
	}

	public String getCodiceRiferimentoAtt() {
		return codiceRiferimentoAtt;
	}
	public void setCodiceRiferimentoAtt(String codiceRiferimentoAtt) {
		this.codiceRiferimentoAtt = codiceRiferimentoAtt;
	}

	public String getDescrizioneAtt() {
		return descrizioneAtt;
	}
	public void setDescrizioneAtt(String descrizioneAtt) {
		this.descrizioneAtt = descrizioneAtt;
	}

	public String getDescrizioneMacroAtt() {
		return descrizioneMacroAtt;
	}
	public void setDescrizioneMacroAtt(String descrizioneMacroAtt) {
		this.descrizioneMacroAtt = descrizioneMacroAtt;
	}

	public int getImpattoAttivita() {
		return impattoAttivita;
	}
	public void setImpattoAttivita(int impattoAttivita) {
		this.impattoAttivita = impattoAttivita;
	}

	public String getNoteAttivita() {
		return noteAttivita;
	}
	public void setNoteAttivita(String noteAttivita) {
		this.noteAttivita = noteAttivita;
	}

	public int getFkValutazioneAtt() {
		return fkValutazioneAtt;
	}
	public void setFkValutazioneAtt(int fkValutazioneAtt) {
		this.fkValutazioneAtt = fkValutazioneAtt;
	}

	public int getFkAuditAtt() {
		return fkAuditAtt;
	}
	public void setFkAuditAtt(int fkAuditAtt) {
		this.fkAuditAtt = fkAuditAtt;
	}


    public static String getEntityFieldName(String dtoFieldName) {
		return dtoFieldName;
    }
    
}
