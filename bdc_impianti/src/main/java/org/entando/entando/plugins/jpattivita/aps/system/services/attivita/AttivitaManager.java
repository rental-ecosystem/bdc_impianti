/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpattivita.aps.system.services.attivita;

import java.util.ArrayList;
import java.util.List;

import org.entando.entando.plugins.jpattivita.aps.system.services.attivita.event.AttivitaChangedEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.agiletec.aps.system.common.AbstractService;
import com.agiletec.aps.system.common.FieldSearchFilter;
import com.agiletec.aps.system.common.model.dao.SearcherDaoPaginatedResult;
import com.agiletec.aps.system.exception.ApsSystemException;
import com.agiletec.aps.system.services.keygenerator.IKeyGeneratorManager;

public class AttivitaManager extends AbstractService implements IAttivitaManager {

	private static final Logger logger =  LoggerFactory.getLogger(AttivitaManager.class);

	@Override
	public void init() throws Exception {
		logger.debug("{} ready.", this.getClass().getName());
	}
 
	@Override
	public Attivita getAttivita(int id) throws ApsSystemException {
		Attivita attivita = null;
		try {
			attivita = this.getAttivitaDAO().loadAttivita(id);
		} catch (Throwable t) {
			logger.error("Error loading attivita with id '{}'", id,  t);
			throw new ApsSystemException("Error loading attivita with id: " + id, t);
		}
		return attivita;
	}

	@Override
	public List<Integer> getAttivitas() throws ApsSystemException {
		List<Integer> attivitas = new ArrayList<Integer>();
		try {
			attivitas = this.getAttivitaDAO().loadAttivitas();
		} catch (Throwable t) {
			logger.error("Error loading Attivita list",  t);
			throw new ApsSystemException("Error loading Attivita ", t);
		}
		return attivitas;
	}

	@Override
	public List<Integer> searchAttivitas(FieldSearchFilter filters[]) throws ApsSystemException {
		List<Integer> attivitas = new ArrayList<Integer>();
		try {
			attivitas = this.getAttivitaDAO().searchAttivitas(filters);
		} catch (Throwable t) {
			logger.error("Error searching Attivitas", t);
			throw new ApsSystemException("Error searching Attivitas", t);
		}
		return attivitas;
	}

	@Override
	public void addAttivita(Attivita attivita) throws ApsSystemException {
		try {
			this.getAttivitaDAO().insertAttivita(attivita);
			this.notifyAttivitaChangedEvent(attivita, AttivitaChangedEvent.INSERT_OPERATION_CODE);
		} catch (Throwable t) {
			logger.error("Error adding Attivita", t);
			throw new ApsSystemException("Error adding Attivita", t);
		}
	}
	
	@Override
	public void addAttivitaList(List<Attivita> attivitaList) throws ApsSystemException {
		try {
			this.getAttivitaDAO().insertAttivitaList(attivitaList);
		} catch (Throwable t) {
			logger.error("Error adding Attivita", t);
			throw new ApsSystemException("Error adding Attivita", t);
		}
	}
 
	@Override
	public void updateAttivita(Attivita attivita) throws ApsSystemException {
		try {
			this.getAttivitaDAO().updateAttivita(attivita);
			this.notifyAttivitaChangedEvent(attivita, AttivitaChangedEvent.UPDATE_OPERATION_CODE);
		} catch (Throwable t) {
			logger.error("Error updating Attivita", t);
			throw new ApsSystemException("Error updating Attivita " + attivita, t);
		}
	}

	@Override
	public void deleteAttivita(int id) throws ApsSystemException {
		try {
			Attivita attivita = this.getAttivita(id);
			this.getAttivitaDAO().removeAttivita(id);
			this.notifyAttivitaChangedEvent(attivita, AttivitaChangedEvent.REMOVE_OPERATION_CODE);
		} catch (Throwable t) {
			logger.error("Error deleting Attivita with id {}", id, t);
			throw new ApsSystemException("Error deleting Attivita with id:" + id, t);
		}
	}


	private void notifyAttivitaChangedEvent(Attivita attivita, int operationCode) {
		AttivitaChangedEvent event = new AttivitaChangedEvent();
		event.setAttivita(attivita);
		event.setOperationCode(operationCode);
		this.notifyEvent(event);
	}

    @SuppressWarnings("rawtypes")
    public SearcherDaoPaginatedResult<Attivita> getAttivitas(FieldSearchFilter[] filters) throws ApsSystemException {
        SearcherDaoPaginatedResult<Attivita> pagedResult = null;
        try {
            List<Attivita> attivitas = new ArrayList<>();
            int count = this.getAttivitaDAO().countAttivitas(filters);

            List<Integer> attivitaNames = this.getAttivitaDAO().searchAttivitas(filters);
            for (Integer attivitaName : attivitaNames) {
                attivitas.add(this.getAttivita(attivitaName));
            }
            pagedResult = new SearcherDaoPaginatedResult<Attivita>(count, attivitas);
        } catch (Throwable t) {
            logger.error("Error searching attivitas", t);
            throw new ApsSystemException("Error searching attivitas", t);
        }
        return pagedResult;
    }

    @Override
    public SearcherDaoPaginatedResult<Attivita> getAttivitas(List<FieldSearchFilter> filters) throws ApsSystemException {
        FieldSearchFilter[] array = null;
        if (null != filters) {
            array = filters.toArray(new FieldSearchFilter[filters.size()]);
        }
        return this.getAttivitas(array);
    }


	protected IKeyGeneratorManager getKeyGeneratorManager() {
		return _keyGeneratorManager;
	}
	public void setKeyGeneratorManager(IKeyGeneratorManager keyGeneratorManager) {
		this._keyGeneratorManager = keyGeneratorManager;
	}

	public void setAttivitaDAO(IAttivitaDAO attivitaDAO) {
		 this._attivitaDAO = attivitaDAO;
	}
	protected IAttivitaDAO getAttivitaDAO() {
		return _attivitaDAO;
	}

	private IKeyGeneratorManager _keyGeneratorManager;
	private IAttivitaDAO _attivitaDAO;
}
