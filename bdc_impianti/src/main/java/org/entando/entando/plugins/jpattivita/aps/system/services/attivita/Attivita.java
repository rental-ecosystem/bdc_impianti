/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpattivita.aps.system.services.attivita;



public class Attivita {


	public int getId_att() {
		return _id_att;
	}
	public void setId_att(int id_att) {
		this._id_att = id_att;
	}

	public String getCodiceRiferimentoAtt() {
		return _codiceRiferimentoAtt;
	}
	public void setCodiceRiferimentoAtt(String codiceRiferimentoAtt) {
		this._codiceRiferimentoAtt = codiceRiferimentoAtt;
	}

	public String getDescrizioneAtt() {
		return _descrizioneAtt;
	}
	public void setDescrizioneAtt(String descrizioneAtt) {
		this._descrizioneAtt = descrizioneAtt;
	}

	public String getDescrizioneMacroAtt() {
		return _descrizioneMacroAtt;
	}
	public void setDescrizioneMacroAtt(String descrizioneMacroAtt) {
		this._descrizioneMacroAtt = descrizioneMacroAtt;
	}

	public int getImpattoAttivita() {
		return _impattoAttivita;
	}
	public void setImpattoAttivita(int impattoAttivita) {
		this._impattoAttivita = impattoAttivita;
	}

	public String getNoteAttivita() {
		return _noteAttivita;
	}
	public void setNoteAttivita(String noteAttivita) {
		this._noteAttivita = noteAttivita;
	}

	public int getFkValutazioneAtt() {
		return _fkValutazioneAtt;
	}
	public void setFkValutazioneAtt(int fkValutazioneAtt) {
		this._fkValutazioneAtt = fkValutazioneAtt;
	}

	public int getFkAuditAtt() {
		return _fkAuditAtt;
	}
	public void setFkAuditAtt(int fkAuditAtt) {
		this._fkAuditAtt = fkAuditAtt;
	}

	
	private int _id_att;
	private String _codiceRiferimentoAtt;
	private String _descrizioneAtt;
	private String _descrizioneMacroAtt;
	private int _impattoAttivita;
	private String _noteAttivita;
	private int _fkValutazioneAtt;
	private int _fkAuditAtt;

}
