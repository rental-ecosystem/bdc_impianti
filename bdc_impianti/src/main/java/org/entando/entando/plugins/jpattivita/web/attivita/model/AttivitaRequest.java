/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpattivita.web.attivita.model;


import javax.validation.constraints.*;
import org.hibernate.validator.constraints.NotBlank;

public class AttivitaRequest {

    @NotNull(message = "attivita.id_att.notBlank")	
	private int id_att;

	@NotBlank(message = "attivita.codiceRiferimentoAtt.notBlank")
	private String codiceRiferimentoAtt;

	@NotBlank(message = "attivita.descrizioneAtt.notBlank")
	private String descrizioneAtt;

	@NotBlank(message = "attivita.descrizioneMacroAtt.notBlank")
	private String descrizioneMacroAtt;

    @NotNull(message = "attivita.impattoAttivita.notBlank")	
	private int impattoAttivita;

	@NotBlank(message = "attivita.noteAttivita.notBlank")
	private String noteAttivita;

    @NotNull(message = "attivita.fkValutazioneAtt.notBlank")	
	private int fkValutazioneAtt;

    @NotNull(message = "attivita.fkAuditAtt.notBlank")	
	private int fkAuditAtt;


	public int getId_att() {
		return id_att;
	}
	public void setId_att(int id_att) {
		this.id_att = id_att;
	}

	public String getCodiceRiferimentoAtt() {
		return codiceRiferimentoAtt;
	}
	public void setCodiceRiferimentoAtt(String codiceRiferimentoAtt) {
		this.codiceRiferimentoAtt = codiceRiferimentoAtt;
	}

	public String getDescrizioneAtt() {
		return descrizioneAtt;
	}
	public void setDescrizioneAtt(String descrizioneAtt) {
		this.descrizioneAtt = descrizioneAtt;
	}

	public String getDescrizioneMacroAtt() {
		return descrizioneMacroAtt;
	}
	public void setDescrizioneMacroAtt(String descrizioneMacroAtt) {
		this.descrizioneMacroAtt = descrizioneMacroAtt;
	}

	public int getImpattoAttivita() {
		return impattoAttivita;
	}
	public void setImpattoAttivita(int impattoAttivita) {
		this.impattoAttivita = impattoAttivita;
	}

	public String getNoteAttivita() {
		return noteAttivita;
	}
	public void setNoteAttivita(String noteAttivita) {
		this.noteAttivita = noteAttivita;
	}

	public int getFkValutazioneAtt() {
		return fkValutazioneAtt;
	}
	public void setFkValutazioneAtt(int fkValutazioneAtt) {
		this.fkValutazioneAtt = fkValutazioneAtt;
	}

	public int getFkAuditAtt() {
		return fkAuditAtt;
	}
	public void setFkAuditAtt(int fkAuditAtt) {
		this.fkAuditAtt = fkAuditAtt;
	}


}
