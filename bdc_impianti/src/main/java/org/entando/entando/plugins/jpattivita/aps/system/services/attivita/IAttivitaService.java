/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpattivita.aps.system.services.attivita;

import java.util.List;

import  org.entando.entando.plugins.jpattivita.aps.system.services.attivita.model.AttivitaDto;
import org.entando.entando.web.common.model.PagedMetadata;
import org.entando.entando.web.common.model.RestListRequest;
import org.entando.entando.plugins.jpattivita.web.attivita.model.AttivitaRequest;

public interface IAttivitaService {

    public String BEAN_NAME = "jpattivitaAttivitaService";

    public PagedMetadata<AttivitaDto> getAttivitas(RestListRequest requestList);

    public AttivitaDto updateAttivita(AttivitaRequest attivitaRequest);

    public AttivitaDto addAttivita(AttivitaRequest attivitaRequest);

    public void removeAttivita(int id);

    public AttivitaDto getAttivita(int  id);

	void addAttivitaList(List<AttivitaRequest> attivitaRequestList);

}

