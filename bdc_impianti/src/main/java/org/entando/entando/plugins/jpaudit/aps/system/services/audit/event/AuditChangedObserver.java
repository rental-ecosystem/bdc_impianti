/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpaudit.aps.system.services.audit.event;

import com.agiletec.aps.system.common.notify.ObserverService;

public interface AuditChangedObserver extends ObserverService {
	
	public void updateFromAuditChanged(AuditChangedEvent event);
	
}
