/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpaudit.aps.system.services.audit;

import  org.entando.entando.plugins.jpaudit.aps.system.services.audit.model.AuditDto;
import org.entando.entando.web.common.model.PagedMetadata;
import org.entando.entando.web.common.model.RestListRequest;
import org.entando.entando.plugins.jpaudit.web.audit.model.AuditRequest;
import org.entando.entando.plugins.jpaudit.web.audit.validator.AuditValidator;

import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import com.agiletec.aps.system.common.FieldSearchFilter;
import com.agiletec.aps.system.exception.ApsSystemException;
import com.agiletec.aps.system.common.model.dao.SearcherDaoPaginatedResult;
import org.entando.entando.aps.system.exception.RestServerError;
import org.entando.entando.web.common.exceptions.ValidationGenericException;
import org.entando.entando.aps.system.services.DtoBuilder;
import org.entando.entando.aps.system.services.IDtoBuilder;
import org.entando.entando.web.common.model.PagedMetadata;
import org.entando.entando.web.common.model.RestListRequest;
import org.entando.entando.aps.system.exception.ResourceNotFoundException;
import org.springframework.validation.BeanPropertyBindingResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;

public class AuditService implements IAuditService {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    private IAuditManager auditManager;
    private IDtoBuilder<Audit, AuditDto> dtoBuilder;


    protected IAuditManager getAuditManager() {
        return auditManager;
    }

    public void setAuditManager(IAuditManager auditManager) {
        this.auditManager = auditManager;
    }

    protected IDtoBuilder<Audit, AuditDto> getDtoBuilder() {
        return dtoBuilder;
    }

    public void setDtoBuilder(IDtoBuilder<Audit, AuditDto> dtoBuilder) {
        this.dtoBuilder = dtoBuilder;
    }

    @PostConstruct
    public void onInit() {
        this.setDtoBuilder(new DtoBuilder<Audit, AuditDto>() {

            @Override
            protected AuditDto toDto(Audit src) {
                AuditDto dto = new AuditDto();
                BeanUtils.copyProperties(src, dto);
                return dto;
            }
        });
    }

    @Override
    public PagedMetadata<AuditDto> getAudits(RestListRequest requestList) {
        try {
            List<FieldSearchFilter> filters = new ArrayList<FieldSearchFilter>(requestList.buildFieldSearchFilters());
            filters
                   .stream()
                   .filter(i -> i.getKey() != null)
                   .forEach(i -> i.setKey(AuditDto.getEntityFieldName(i.getKey())));

            SearcherDaoPaginatedResult<Audit> audits = this.getAuditManager().getAudits(filters);
            List<AuditDto> dtoList = dtoBuilder.convert(audits.getList());

            PagedMetadata<AuditDto> pagedMetadata = new PagedMetadata<>(requestList, audits);
            pagedMetadata.setBody(dtoList);

            return pagedMetadata;
        } catch (Throwable t) {
            logger.error("error in search audits", t);
            throw new RestServerError("error in search audits", t);
        }
    }

    @Override 
    public AuditDto updateAudit(AuditRequest auditRequest) {
        try {
	        Audit audit = this.getAuditManager().getAudit(auditRequest.getIdAudit());
	        if (null == audit) {
	            throw new ResourceNotFoundException(AuditValidator.ERRCODE_AUDIT_NOT_FOUND, "audit", String.valueOf(auditRequest.getIdAudit()));
	        }
        	BeanUtils.copyProperties(auditRequest, audit);
            BeanPropertyBindingResult validationResult = this.validateForUpdate(audit);
            if (validationResult.hasErrors()) {
                throw new ValidationGenericException(validationResult);
            }
            this.getAuditManager().updateAudit(audit);
            return this.getDtoBuilder().convert(audit);
        } catch (ApsSystemException e) {
            logger.error("Error updating audit {}", auditRequest.getIdAudit(), e);
            throw new RestServerError("error in update audit", e);
        }
    }

    @Override
    public AuditDto addAudit(AuditRequest auditRequest) {
        try {
            Audit audit = this.createAudit(auditRequest);
            BeanPropertyBindingResult validationResult = this.validateForAdd(audit);
            if (validationResult.hasErrors()) {
                throw new ValidationGenericException(validationResult);
            }
            this.getAuditManager().addAudit(audit);
            AuditDto dto = this.getDtoBuilder().convert(audit);
            return dto;
        } catch (ApsSystemException e) {
            logger.error("Error adding a audit", e);
            throw new RestServerError("error in add audit", e);
        }
    }

    @Override
    public void removeAudit(int  id) {
        try {
            Audit audit = this.getAuditManager().getAudit(id);
            if (null == audit) {
                logger.info("audit {} does not exists", id);
                return;
            }
            BeanPropertyBindingResult validationResult = this.validateForDelete(audit);
            if (validationResult.hasErrors()) {
                throw new ValidationGenericException(validationResult);
            }
            this.getAuditManager().deleteAudit(id);
        } catch (ApsSystemException e) {
            logger.error("Error in delete audit {}", id, e);
            throw new RestServerError("error in delete audit", e);
        }
    }

    @Override
    public AuditDto getAudit(int  id) {
        try {
	        Audit audit = this.getAuditManager().getAudit(id);
	        if (null == audit) {
	            logger.warn("no audit found with code {}", id);
	            throw new ResourceNotFoundException(AuditValidator.ERRCODE_AUDIT_NOT_FOUND, "audit", String.valueOf(id));
	        }
	        AuditDto dto = this.getDtoBuilder().convert(audit);
	        return dto;
        } catch (ApsSystemException e) {
            logger.error("Error loading audit {}", id, e);
            throw new RestServerError("error in loading audit", e);
        }
    }

    private Audit createAudit(AuditRequest auditRequest) {
        Audit audit = new Audit();
        BeanUtils.copyProperties(auditRequest, audit);
        return audit;
    }


    protected BeanPropertyBindingResult validateForAdd(Audit audit) {
        BeanPropertyBindingResult errors = new BeanPropertyBindingResult(audit, "audit");
        return errors;
    }

    protected BeanPropertyBindingResult validateForDelete(Audit audit) {
        BeanPropertyBindingResult errors = new BeanPropertyBindingResult(audit, "audit");
        return errors;
    }

    protected BeanPropertyBindingResult validateForUpdate(Audit audit) {
        BeanPropertyBindingResult errors = new BeanPropertyBindingResult(audit, "audit");
        return errors;
    }

}

