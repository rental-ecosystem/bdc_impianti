/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpaudit.aps.system.services.audit;

import java.util.Date;

public class Audit {

	public int getIdAudit() {
		return _idAudit;
	}
	public void setIdAudit(int idAudit) {
		this._idAudit = idAudit;
	}

	public int getValoreVerificaAudit() {
		return _valoreVerificaAudit;
	}
	public void setValoreVerificaAudit(int valoreVerificaAudit) {
		this._valoreVerificaAudit = valoreVerificaAudit;
	}

	public String getIspettoreAudit() {
		return _ispettoreAudit;
	}
	public void setIspettoreAudit(String ispettoreAudit) {
		this._ispettoreAudit = ispettoreAudit;
	}

	public String getNoteGeneraliAudit() {
		return _noteGeneraliAudit;
	}
	public void setNoteGeneraliAudit(String noteGeneraliAudit) {
		this._noteGeneraliAudit = noteGeneraliAudit;
	}

	public Date getDataAudit() {
		return _dataAudit;
	}
	public void setDataAudit(Date dataAudit) {
		this._dataAudit = dataAudit;
	}

	public Date getDataInserimentoAudit() {
		return _dataInserimentoAudit;
	}
	public void setDataInserimentoAudit(Date dataInserimentoAudit) {
		this._dataInserimentoAudit = dataInserimentoAudit;
	}

	public Date getDataModificaAudit() {
		return _dataModificaAudit;
	}
	public void setDataModificaAudit(Date dataModificaAudit) {
		this._dataModificaAudit = dataModificaAudit;
	}

	public String getFkCodiceSito() {
		return _fkCodiceSito;
	}
	public void setFkCodiceSito(String fkCodiceSito) {
		this._fkCodiceSito = fkCodiceSito;
	}

	
	private int _idAudit;
	private int _valoreVerificaAudit;
	private String _ispettoreAudit;
	private String _noteGeneraliAudit;
	private Date _dataAudit;
	private Date _dataInserimentoAudit;
	private Date _dataModificaAudit;
	private String _fkCodiceSito;

}
