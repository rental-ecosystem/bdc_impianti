/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpaudit.aps.system.services.audit.model;

import java.util.Date;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import org.entando.entando.web.common.json.JsonDateDeserializer;
import org.entando.entando.web.common.json.JsonDateSerializer;

public class AuditDto {

	private int idAudit;
	private int valoreVerificaAudit;
	private String ispettoreAudit;
	private String noteGeneraliAudit;
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
	private Date dataAudit;
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
	private Date dataInserimentoAudit;
    @JsonSerialize(using = JsonDateSerializer.class)
    @JsonDeserialize(using = JsonDateDeserializer.class)
	private Date dataModificaAudit;
	private String fkCodiceSito;

	public int getIdAudit() {
		return idAudit;
	}
	public void setIdAudit(int idAudit) {
		this.idAudit = idAudit;
	}

	public int getValoreVerificaAudit() {
		return valoreVerificaAudit;
	}
	public void setValoreVerificaAudit(int valoreVerificaAudit) {
		this.valoreVerificaAudit = valoreVerificaAudit;
	}

	public String getIspettoreAudit() {
		return ispettoreAudit;
	}
	public void setIspettoreAudit(String ispettoreAudit) {
		this.ispettoreAudit = ispettoreAudit;
	}

	public String getNoteGeneraliAudit() {
		return noteGeneraliAudit;
	}
	public void setNoteGeneraliAudit(String noteGeneraliAudit) {
		this.noteGeneraliAudit = noteGeneraliAudit;
	}

	public Date getDataAudit() {
		return dataAudit;
	}
	public void setDataAudit(Date dataAudit) {
		this.dataAudit = dataAudit;
	}

	public Date getDataInserimentoAudit() {
		return dataInserimentoAudit;
	}
	public void setDataInserimentoAudit(Date dataInserimentoAudit) {
		this.dataInserimentoAudit = dataInserimentoAudit;
	}

	public Date getDataModificaAudit() {
		return dataModificaAudit;
	}
	public void setDataModificaAudit(Date dataModificaAudit) {
		this.dataModificaAudit = dataModificaAudit;
	}

	public String getFkCodiceSito() {
		return fkCodiceSito;
	}
	public void setFkCodiceSito(String fkCodiceSito) {
		this.fkCodiceSito = fkCodiceSito;
	}


    public static String getEntityFieldName(String dtoFieldName) {
		return dtoFieldName;
    }
    
}
