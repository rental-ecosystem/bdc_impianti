--
-- table structure for table `formule`
--

drop table if exists `formule`;
create table `formule` (
  `id_formula` int(11) not null ,
  `descrizioneformula` varchar(1000) default null,
  `valoreformula` varchar(1000) default null,
  primary key (`id_formula`)
) engine=innodb default charset=latin1;

--
-- table structure for table `logimportaudit`
--

drop table if exists `logimportaudit`;
create table `logimportaudit` (
  `id_log` int(11) not null ,
  `nomefile` varchar(1000) default null,
  `data_inserimentofile` datetime default current_timestamp,
  primary key (`id_log`)
) engine=innodb default charset=latin1;

--
-- table structure for table `sito`
--

drop table if exists `sito`;
create table `sito` (
  `id_sito` int(11) not null ,
  `codicesito` varchar(45) not null unique,
  `descrizionesito` varchar(1000) default null,
  `regionesito` varchar(100) default null,
  primary key (`id_sito`)
) engine=innodb default charset=latin1;

--
-- table structure for table `valutazioneattivita`
--

drop table if exists `valutazioneattivita`;
create table `valutazioneattivita` (
  `id_val` int(11) not null,
  `descrizioneval` varchar(1000) default null,
  `prioritaval` int(11) default null,
  primary key (`id_val`)
) engine=innodb default charset=latin1;

lock tables `valutazioneattivita` write;
insert into `valutazioneattivita` values (1,'pienamente soddisfacente',1),(2,'soddisfacente',2),(3,'non soddisfacente',3),(4,'n/a - asset non presente o attivita non prevista',4);
unlock tables;

--
-- table structure for table `auditsito`
--

drop table if exists `auditsito`;
create table `auditsito` (
  `id_audit` int(11) not null ,
  `valoreverificaaudit` int(11) default null,
  `ispettoreaudit` varchar(500) default null,
  `notegeneraliaudit` varchar(10000) default null,
  `datainserimentoaudit` datetime default current_timestamp,
  `dataaudit` date,
  `datamodificaaudit` datetime default null,
  `fksito` int(11) default null,
  primary key (`id_audit`),
  key `fksitoauditidx` (`fksito`),
  constraint `fksitoaudit` foreign key (`fksito`) references `sito` (`id_sito`) on delete no action on update no action
) engine=innodb default charset=latin1;

--
-- table structure for table `attivita`
--

drop table if exists `attivita`;
create table `attivita` (
  `id_att` int(11) not null ,
  `codicerifatt` varchar(45) default null,
  `descrizioneatt` varchar(5000) default null,
  `descrizionemacroatt` varchar(5000) default null,
  `impattoatt` int(11) default null,
  `noteattivita` varchar(5000) default null,
  `fkvalutazioneatt` int(11) default null,
  `fkauditatt` int(11) default null,
  primary key (`id_att`),
  key `fkvalutazioneattivitaidx` (`fkvalutazioneatt`),
  key `fkauditattivitaidx` (`fkauditatt`),
  constraint `fkauditattivita` foreign key (`fkauditatt`) references `auditsito` (`id_audit`) on delete no action on update no action,
  constraint `fkvalutazioneattivita` foreign key (`fkvalutazioneatt`) references `valutazioneattivita` (`id_val`) on delete no action on update no action
) engine=innodb default charset=latin1;