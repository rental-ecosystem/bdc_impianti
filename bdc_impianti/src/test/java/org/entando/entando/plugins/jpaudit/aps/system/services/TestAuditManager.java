/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpaudit.aps.system.services;

import org.entando.entando.plugins.jpaudit.aps.JpauditBaseTestCase;
import org.entando.entando.plugins.jpaudit.aps.system.services.audit.IAuditManager;

public class TestAuditManager extends JpauditBaseTestCase {

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.init();
	}
	
	public void testGetAudit() {
		//TODO complete test
		assertNotNull(this._auditManager);
	}

	public void testGetAudits() {
		//TODO complete test
		assertNotNull(this._auditManager);
	}
	
	public void testSearchAudits() {
		//TODO complete test
		assertNotNull(this._auditManager);
	}

	public void testAddAudit() {
		//TODO complete test
		assertNotNull(this._auditManager);
	}

	public void testUpdateAudit() {
		//TODO complete test
		assertNotNull(this._auditManager);
	}

	public void testDeleteAudit() {
		//TODO complete test
		assertNotNull(this._auditManager);
	}
	
	private void init() {
		//TODO add the spring bean id as constant
		this._auditManager = (IAuditManager) this.getService("jpauditAuditManager");
	}
	
	private IAuditManager _auditManager;
}

