/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpaudit.aps;


import org.entando.entando.plugins.jpaudit.JpauditConfigTestUtils;

import com.agiletec.ConfigTestUtils;
import com.agiletec.aps.BaseTestCase;

public class JpauditBaseTestCase extends BaseTestCase {

	@Override
	protected ConfigTestUtils getConfigUtils() {
		return new JpauditConfigTestUtils();
	}

	
}
