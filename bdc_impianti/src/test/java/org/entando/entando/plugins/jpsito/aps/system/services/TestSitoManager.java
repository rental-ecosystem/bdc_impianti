/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpsito.aps.system.services;

import org.entando.entando.plugins.jpsito.aps.JpsitoBaseTestCase;
import org.entando.entando.plugins.jpsito.aps.system.services.sito.ISitoManager;

public class TestSitoManager extends JpsitoBaseTestCase {

	@Override
	protected void setUp() throws Exception {
		super.setUp();
		this.init();
	}
	
	public void testGetSito() {
		//TODO complete test
		assertNotNull(this._sitoManager);
	}

	public void testGetSitos() {
		//TODO complete test
		assertNotNull(this._sitoManager);
	}
	
	public void testSearchSitos() {
		//TODO complete test
		assertNotNull(this._sitoManager);
	}

	public void testAddSito() {
		//TODO complete test
		assertNotNull(this._sitoManager);
	}

	public void testUpdateSito() {
		//TODO complete test
		assertNotNull(this._sitoManager);
	}

	public void testDeleteSito() {
		//TODO complete test
		assertNotNull(this._sitoManager);
	}
	
	private void init() {
		//TODO add the spring bean id as constant
		this._sitoManager = (ISitoManager) this.getService("jpsitoSitoManager");
	}
	
	private ISitoManager _sitoManager;
}

