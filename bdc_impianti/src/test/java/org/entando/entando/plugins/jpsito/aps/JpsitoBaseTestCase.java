/*
 *
 * <Your licensing text here>
 *
 */
package org.entando.entando.plugins.jpsito.aps;


import org.entando.entando.plugins.jpsito.JpsitoConfigTestUtils;

import com.agiletec.ConfigTestUtils;
import com.agiletec.aps.BaseTestCase;

public class JpsitoBaseTestCase extends BaseTestCase {

	@Override
	protected ConfigTestUtils getConfigUtils() {
		return new JpsitoConfigTestUtils();
	}

	
}
